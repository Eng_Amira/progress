json.extract! visitor_service, :id, :service_id, :name, :company_type, :email, :phone, :address, :birth_date, :job_title, :adminstrator_name, :previous_work, :details, :created_at, :updated_at
json.url visitor_service_url(visitor_service, format: :json)
