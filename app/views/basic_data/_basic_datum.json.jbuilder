json.extract! basic_datum, :id, :model_id, :user_id, :created_at, :updated_at
json.url basic_datum_url(basic_datum, format: :json)
