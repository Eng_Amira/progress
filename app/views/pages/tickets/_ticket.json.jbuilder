json.extract! ticket, :id, :user_id, :ticket_id, :ticket_number, :title, :description, :status, :type, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
