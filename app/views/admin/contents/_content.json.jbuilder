json.extract! content, :id, :key, :category, :title, :description, :created_at, :updated_at
json.url content_url(content, format: :json)
