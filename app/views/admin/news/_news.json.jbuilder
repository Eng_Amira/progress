json.extract! news, :id, :title_en, :title_ar, :description_en, :description_ar, :status, :created_at, :updated_at
json.url news_url(news, format: :json)
