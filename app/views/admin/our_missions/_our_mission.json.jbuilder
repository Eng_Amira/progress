json.extract! our_mission, :id, :title, :description, :created_at, :updated_at
json.url our_mission_url(our_mission, format: :json)
