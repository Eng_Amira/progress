json.extract! required_file, :id, :user_id, :description, :created_at, :updated_at
json.url required_file_url(required_file, format: :json)
