json.extract! audit_log, :id, :user_id, :added_by, :foreign_id, :action_type, :meta_data, :ip_address, :user_agent, :created_at, :updated_at
json.url audit_log_url(audit_log, format: :json)
