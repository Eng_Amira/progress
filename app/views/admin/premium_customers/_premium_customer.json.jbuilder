json.extract! premium_customer, :id, :key, :name, :details, :status, :created_at, :updated_at
json.url premium_customer_url(premium_customer, format: :json)
