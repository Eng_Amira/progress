class UserMailer < ApplicationMailer
    default from: "progress <no-reply@progressconsult.net>"

    # send a mail in case of signing in from different ip     
    def send_signin_data(mail,password)       
       @email = mail
       @password = password         
             
         mail to: @email, subject: 'Welcome to My Awesome Site'
    end

    # send a mail to confirm sign in
    def confirmsignin(user_mail,username,token)
       
        @email = user_mail
        @username = username
        @token = token   
              
          mail to: @email, subject: t('confirm_sign_in')
    end


    # send a mail to reenable the locked account
    def unlockaccount(user,token)
       
        @email = user.email
        @token = token   
        @id = user.id
              
          mail to: user.email, subject: t('Your_account_locked')
    end

    # send a mail to user
    def admin_reply(name,email,code,subject,reply)

        @name = name
        @email = email  
        @code = code 
        @reply = reply 
        @subject = subject 
              
          mail to: @email, subject: @subject 
    end

    # send a mail to reenable the locked account
    def sendmessage(email,name,msg,send_to)
       
        @email = email  
        @name = name 
        @msg = msg 
        @send_to = send_to
        @subject = t('email_from_visitor')
              
          mail to: @send_to, subject: @subject 
    end

    def activate_account(email,username)
        @email = email 
        @name = username         
        @subject = t('account_has_activated')
              
          mail to: @email, subject: @subject 
    end

    def new_account(email,username)
        @email = email 
        @name = username         
        @subject = t('account_has_created')
              
          mail to: @email, subject: @subject 
    end

    def newvisitor_request(email,username,code)
        @email = email 
        @name = username 
        @code = code         
        @subject = t('request_has_created')
              
          mail to: @email, subject: @subject 
    end


   

      
end
