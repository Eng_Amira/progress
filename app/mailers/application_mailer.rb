class ApplicationMailer < ActionMailer::Base
  default from: 'progresspioneers.com'
  layout 'mailer'
end
