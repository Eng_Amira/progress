class BasicDataController < ApplicationController
  before_action :set_basic_datum, only: [:show, :edit, :update, :destroy]
  before_action :require_login 
  layout 'user'


  # GET /basic_data
  # GET /basic_data.json
  def index
    @basic_data = BasicDatum.where(user_id: current_user.id).all
  end

  # GET /basic_data/1
  # GET /basic_data/1.json
  def show
  end

  # GET /basic_data/new
  def new
    @basic_datum = BasicDatum.new
  end

  # GET /basic_data/1/edit
  def edit
  end

  # POST /basic_data
  # POST /basic_data.json
  def create
    @basic_datum = BasicDatum.new(basic_datum_params)
    @basic_datum.user_id = current_user.id

    respond_to do |format|
      if @basic_datum.save
        AuditLog.create(user_id: current_user.id, added_by: current_user.id, foreign_id: @basic_datum.id, action_type: "upload basic data", meta_data: "User has uploaded basic data", ip_address: request.remote_ip, user_agent: request.user_agent)
        format.html { redirect_to basic_data_path, notice: 'Basic datum was successfully created.' }
        format.json { render :show, status: :created, location: @basic_datum }
      else
        format.html { render :new }
        format.json { render json: @basic_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /basic_data/1
  # PATCH/PUT /basic_data/1.json
  def update
    respond_to do |format|
      if @basic_datum.update(basic_datum_params)
        AuditLog.create(user_id: current_user.id, added_by: current_user.id, foreign_id: @basic_datum.id, action_type: "Edit basic data", meta_data: "User has Edited basic data", ip_address: request.remote_ip, user_agent: request.user_agent)
        format.html { redirect_to basic_data_path, notice: 'Basic datum was successfully updated.' }
        format.json { render :show, status: :ok, location: @basic_datum }
      else
        format.html { render :edit }
        format.json { render json: @basic_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /basic_data/1
  # DELETE /basic_data/1.json
  def destroy
    AuditLog.create(user_id: current_user.id, added_by: current_user.id, foreign_id: @basic_datum.id, action_type: "Delete basic data", meta_data: "User has deleted basic data", ip_address: request.remote_ip, user_agent: request.user_agent)
    @basic_datum.destroy
    respond_to do |format|
      format.html { redirect_to basic_data_url, notice: 'Basic datum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_basic_datum
      @basic_datum = BasicDatum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def basic_datum_params
      params.require(:basic_datum).permit(:model_id, :user_id, :status, :file)
    end
end
