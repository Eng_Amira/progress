class ErrorsLogsController < ApplicationController
  before_action :set_errors_log, only: [:show, :edit, :update, :destroy]

  # GET /errors_logs
  # GET /errors_logs.json
  def index
    @errors_logs = ErrorsLog.all
  end

  # GET /errors_logs/1
  # GET /errors_logs/1.json
  def show
  end

  # GET /errors_logs/new
  def new
    @errors_log = ErrorsLog.new
  end

  # GET /errors_logs/1/edit
  def edit
  end

  # POST /errors_logs
  # POST /errors_logs.json
  def create
    @errors_log = ErrorsLog.new(errors_log_params)

    respond_to do |format|
      if @errors_log.save
        format.html { redirect_to @errors_log, notice: 'Errors log was successfully created.' }
        format.json { render :show, status: :created, location: @errors_log }
      else
        format.html { render :new }
        format.json { render json: @errors_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /errors_logs/1
  # PATCH/PUT /errors_logs/1.json
  def update
    respond_to do |format|
      if @errors_log.update(errors_log_params)
        format.html { redirect_to @errors_log, notice: 'Errors log was successfully updated.' }
        format.json { render :show, status: :ok, location: @errors_log }
      else
        format.html { render :edit }
        format.json { render json: @errors_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /errors_logs/1
  # DELETE /errors_logs/1.json
  def destroy
    @errors_log.destroy
    respond_to do |format|
      format.html { redirect_to errors_logs_url, notice: 'Errors log was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_errors_log
      @errors_log = ErrorsLog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def errors_log_params
      params.require(:errors_log).permit(:user_id, :code, :message, :location)
    end
end
