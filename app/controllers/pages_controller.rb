class PagesController < ApplicationController
  layout 'site'
  def homepage
    render layout: false
  end

  def visitor_login
    
    #if current_user && current_user.role_id == "user"
      #redirect_to root_path, notice: 'You are already signed in'
    #else
      @missions= OurMission.all
      #@missions1 = @missions.all.where(:id => 1..4).order('id ASC')
      

      @premium_customers = PremiumCustomer.all
      @customer_opinions = @premium_customers.where(:category => "customer_opinions").all
      @our_partners = @premium_customers.where(:category => "our_partners").all
      @contacts = Content.where(:category => 5, :key => "contacts").first


      render layout: false
    #end
  end

  def mission
    @mission = OurMission.where("id =?", params[:id].to_i).first
  end

  def contact_us
    
    @address = Content.where(:category => 5, :key => "Address").first
    @contacts = Content.where(:category => 5, :key => "contacts").first
    @map = Content.where(:category => 5, :key => "map").first
  end

  def about_us
    @about_us = Content.where(:key => 'About Us').first
  end

  def our_company
    @about_us = Content.where(:key => 'Our Company').first
  end

  def human_cadres
    @human_cadres = Content.where("category =?", 2).first
  end

  def services
    @service = Content.where("id =?", params[:id].to_i).first
  end

  def customers
    @customer = Content.where("id =?", params[:id].to_i).first
  end

  def news
    @news = New.all
  end

  def details
    @new = New.where("id =?", params[:id].to_i).first
  end

  def disclaimer
    @disclaimer = Content.where(:key => 'disclaimer').first
  end

  def terms_of_use
    @terms_of_use = Content.where(:key => 'terms_of_use').first
  end

  def privacy_policy
    @privacy_policy = Content.where(:key => 'privacy_policy').first
  end

  def change_lang
    session[:locale] = params[:locale]
    redirect_back fallback_location: root_path
  end

  def home
    render layout: 'user'
  end

  def error
    render :layout => false
  end

end
