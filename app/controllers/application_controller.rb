class ApplicationController < ActionController::Base
  include Clearance::Controller
  before_action :check2fa ,except: [:confirm_signin,:resend_confirmation_code]
  before_action :set_locale
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  protect_from_forgery with: :exception



  #layout :set_layout

  def set_layout
    if current_user and current_user.role_id == "admin"
      'admin'
    elsif current_user and current_user.role_id == "user"
      'user'
    else
      'site'
    end
  end

  def check2fa
    if  session[:verify_code] == false
      if current_user
        redirect_to confirm_signin_path
      else 
        render template: "sessions/new"
      end
    end
  end



  def set_locale
    #if current_user      
      #I18n.locale = current_user.locale.to_s
    #else
      if session[:locale] == nil
        session[:locale] = "ar"
      end
      I18n.locale = session[:locale]
    #end 
    if I18n.locale.to_s == "en"
      @website_name = "Progress Pioneers"
    else
      @website_name = "رواد التقدم"      
    end     
  end

  def record_not_found
    redirect_to pages_error_path
  end

end
