class Admin::VisitorServicesController < Admin::AdminController

  before_action :set_visitor_service, only: [:show, :edit, :update, :destroy]
  before_action :require_login , except: [:new, :create]


  # GET /visitor_services
  # GET /visitor_services.json
  def index
    if (current_user.role_id == "admin")
      @visitor_services = VisitorService.all
      respond_to do |format|
        format.html
        format.csv { send_data @visitor_services.to_csv }
        format.xls { send_data @visitor_services.to_csv(col_sep: "\t") }
      end
    else
      redirect_to root_path, notice: 'Not allowed.'
    end
  end

  def search
    @visitor_services = VisitorService.all
    @visitor_services = @visitor_services.where(email: params[:emailsearch]) if  params[:emailsearch].present?
    @visitor_services = @visitor_services.where(["name LIKE ?","%#{params[:companysearch]}%"]) if  params[:companysearch].present?
    @visitor_services = @visitor_services.where(service_id: params[:servicetyp]) if  params[:servicetyp].present?
    @visitor_services = @visitor_services.where(status: params[:status]) if  params[:status].present?
    @visitor_services = @visitor_services.where(code: params[:search]) if  params[:search].present?
    @visitor_services = @visitor_services.where("created_at > ?", params[:searchtime])   if  params[:searchtime].present?
  end

  # GET /visitor_services/1
  # GET /visitor_services/1.json
  def show
    unless (current_user.role_id == "admin" )
      redirect_to root_path, notice: 'Not allowed.' 
    end
  end

  # GET /visitor_services/new
  def new
    if (current_user)    
      redirect_to root_path, notice: 'Not allowed.' 
    end
    @visitor_service = VisitorService.new
  end

  # GET /visitor_services/1/edit
  def edit
    unless (current_user.role_id == "admin" )
      redirect_to root_path, notice: 'Not allowed.' 
    end
  end

  # POST /visitor_services
  # POST /visitor_services.json
  def create
    if (current_user)
     redirect_to root_path, notice: 'Not allowed.' 
    end
    @visitor_service = VisitorService.new(visitor_service_params)

    respond_to do |format|
      if @visitor_service.save
        format.html { redirect_to root_path, notice: 'Visitor service was successfully created with number: ' +  @visitor_service.code}
        format.json { render :show, status: :created, location: @visitor_service }
      else
        format.html { render :new }
        format.json { render json: @visitor_service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /visitor_services/1
  # PATCH/PUT /visitor_services/1.json
  def update
    unless (current_user.role_id == "admin" )
      redirect_to root_path, notice: 'Not allowed.' 
    end
      if @visitor_service.update(visitor_service_params)
        if (params[:visitor_service][:reply])
          @this_service = VisitorService.find_by(id: @visitor_service.id)
          MailingJob.perform_async(name: @visitor_service.name,email: params[:visitor_service][:emailto],code: @this_service.code,subject: params[:subject],reply: params[:visitor_service][:reply])
          #UserMailer.admin_reply(@this_service.email,@this_service.code,params[:visitor_service][:reply]).deliver_later
        end
        redirect_to(admin_visitor_service_path(@visitor_service),notice:t("successfully updated"))
      else
         render :edit
      end
  end

  # DELETE /visitor_services/1
  # DELETE /visitor_services/1.json
  def destroy
    unless (current_user.role_id == "admin" )
      redirect_to root_path, notice: 'Not allowed.' 
    end
    @visitor_service.destroy
    respond_to do |format|
      format.html { redirect_to admin_visitor_services_url, notice: 'Visitor service was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_visitor_service
      @visitor_service = VisitorService.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def visitor_service_params
      params.require(:visitor_service).permit(:service_id, :name, :company_type, :email, :phone, :address, :birth_date, :job_title, :adminstrator_name, :previous_work, :details, :code, :status, :reply, :file)
    end
end
