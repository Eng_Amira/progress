class Admin::OurMissionsController < Admin::AdminController
  before_action :set_our_mission, only: [:show, :edit, :update, :destroy]

  # GET /our_missions
  # GET /our_missions.json
  def index
    @our_missions = OurMission.all
  end

  # GET /our_missions/1
  # GET /our_missions/1.json
  def show
  end

  # GET /our_missions/new
  def new
    @our_mission = OurMission.new
  end

  # GET /our_missions/1/edit
  def edit
  end

  # POST /our_missions
  # POST /our_missions.json
  def create
    @our_mission = OurMission.new(our_mission_params)

    respond_to do |format|
      if @our_mission.save
        format.html { redirect_to admin_our_mission_path(@our_mission), notice: t('Data was successfully created') }
        format.json { render :show, status: :created, location: @our_mission }
      else
        format.html { render :new }
        format.json { render json: @our_mission.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /our_missions/1
  # PATCH/PUT /our_missions/1.json
  def update
    respond_to do |format|
      if @our_mission.update(our_mission_params)
        format.html { redirect_to admin_our_mission_path(@our_mission), notice: t('Data was successfully updated') }
        format.json { render :show, status: :ok, location: @our_mission }
      else
        format.html { render :edit }
        format.json { render json: @our_mission.errors, status: :unprocessable_entity }
      end
    end
  end

  def delete_mission_image
    @missionimage = OurMission.find(params[:id])
    @missionimage.images.find(params[:attachment_id]).purge
    redirect_to admin_our_mission_path(@missionimage), notice: t('Image was deleted')
  end

  # DELETE /our_missions/1
  # DELETE /our_missions/1.json
  def destroy
    @our_mission.images.each do |image| 
      image.purge_later
    end
    @our_mission.destroy
    respond_to do |format|
      format.html { redirect_to admin_our_missions_url, notice: t('Data was successfully deleted') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_our_mission
      @our_mission = OurMission.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def our_mission_params
      params.require(:our_mission).permit(:title_ar, :description_ar,:title_en, :description_en, :small_pic,images: [])
    end
end
