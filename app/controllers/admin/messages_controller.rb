class Admin::MessagesController < Admin::AdminController
  before_action :set_message, only: [:show, :edit, :update, :destroy]

  # GET /tickets
  # GET /tickets.json
  def index
    @tickets = Message.where(ticket_id:  nil).includes(:user).all
  end

  def ticket_search
    @tickets = Message.where(ticket_id:  nil).includes(:user).all
    @tickets = @tickets.where(email: params[:emailsearch]) if  params[:emailsearch].present?
    @tickets = @tickets.where(ticket_number: params[:ticketnumbersearch]) if  params[:ticketnumbersearch].present?
    @tickets = @tickets.where(status: params[:status]) if  params[:status].present?
    @tickets = @tickets.where(ticket_type: params[:ticket_type]) if  params[:ticket_type].present?

  
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
    @ticket_replies = Message.where(ticket_id:  params[:id]).includes(:user).all

  end

  # GET /tickets/new
  def new
    @ticket = Message.new
  end

  # GET /tickets/1/edit
  def edit
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Message.new(message_params)
    @ticket.ticket_number =  Random.new.rand(111111..999999)
    @ticket.status = "opened"
    @ticket_id = params[:message][:ticket_id]
    if params[:message][:ticket_type] == nil
      @ticket.ticket_type = "created_by_admin"
    end

    respond_to do |format|
      if @ticket.save
        if @ticket_id == nil
          format.html { redirect_to admin_message_path(@ticket), notice: 'Ticket was successfully created.' }
        else
          format.html { redirect_to admin_message_path(@ticket_id), notice: 'Reply was successfully created.' }
        end
        format.json { render :show, status: :created, location: @ticket }
      else
        format.html { render :new }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    respond_to do |format|
      if @ticket.update(message_params)
        format.html { redirect_to admin_message_path(@ticket), notice: 'Ticket was successfully updated.' }
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.html { render :edit }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to admin_messages_url, notice: 'Ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @ticket = Message.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:user_id, :ticket_id, :ticket_number, :title, :description, :status, :ticket_type)
    end
end
