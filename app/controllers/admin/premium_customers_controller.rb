class Admin::PremiumCustomersController < Admin::AdminController

  before_action :set_premium_customer, only: [:show, :edit, :update, :destroy]

  # GET /premium_customers
  # GET /premium_customers.json
  def index
    @premium_customers = PremiumCustomer.where(:category => params[:type].to_s).all
  end


  # GET /premium_customers/1
  # GET /premium_customers/1.json
  def show
  end

  # GET /premium_customers/new
  def new
    @premium_customer = PremiumCustomer.new
  end

  # GET /premium_customers/1/edit
  def edit
  end

  # POST /premium_customers
  # POST /premium_customers.json
  def create
    @premium_customer = PremiumCustomer.new(premium_customer_params)

      if @premium_customer.save
        redirect_to admin_premium_customer_path(@premium_customer), notice: 'Premium customer was successfully created.'
      else
        render :new 
      end
  end

  # PATCH/PUT /premium_customers/1
  # PATCH/PUT /premium_customers/1.json
  def update
      if @premium_customer.update(premium_customer_params)
        redirect_to admin_premium_customer_path(@premium_customer), notice: t('Data was successfully updated')
      else
        render :edit
      end
  end

  def delete_customer_image
    @customerimage = PremiumCustomer.find(params[:id])
    @customerimage.image.purge
    redirect_to admin_premium_customer_path(@customerimage), notice: t('Image was deleted')
  end

  # DELETE /premium_customers/1
  # DELETE /premium_customers/1.json
  def destroy
    @premium_customer.image.purge_later
    @premium_customer.destroy
      redirect_to admin_premium_customers_url, notice: 'Premium customer was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_premium_customer
      @premium_customer = PremiumCustomer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def premium_customer_params
      params.require(:premium_customer).permit(:key, :name_ar, :name_en, :description_ar, :description_en, :status, :category, :image)
    end
end
