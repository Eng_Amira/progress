class Admin::UsersBasicDataController < Admin::AdminController
  before_action :set_users_basic_datum, only: [:show, :edit, :update, :destroy]

  # GET /tickets
  # GET /tickets.json
  def index
    @basic_data = UsersBasicDatum.includes(:user).all
  end

  # GET /basic_data/1/edit
  def edit
    @users_basic_datum = UsersBasicDatum.where(id: params[:id]).first     
  end


  # PATCH/PUT /basic_data/1
  # PATCH/PUT /basic_data/1.json
  def update
    respond_to do |format|
      @users_basic_datum = UsersBasicDatum.where(id: params[:id]).first 

      if @users_basic_datum.update(users_basic_datum_params)
        format.html { redirect_to admin_users_basic_data_path, notice: 'basic data was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_basic_datum }
      else
        format.html { render :edit }
        format.json { render json: @users_basic_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /basic_data/1
  # DELETE /basic_data/1.json
  def destroy
    @users_basic_datum.destroy
    respond_to do |format|
      format.html { redirect_to admin_users_basic_data_path, notice: 'Data was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_basic_datum
      @users_basic_datum = UsersBasicDatum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_basic_datum_params
      params.require(:users_basic_datum).permit(:model_id, :user_id, :status, :file)
    end
end


