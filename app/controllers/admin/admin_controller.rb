class Admin::AdminController < ApplicationController  
   
      layout 'admin'
      before_action :require_login, :check_admin_role, :set_locale
  
      def check_admin_role
        unless current_user and current_user.role_id == "admin"
          redirect_to root_path, notice: 'Not authorized To access this area'
        end
      end

      def set_locale
          if current_user      
            I18n.locale = current_user.locale.to_s
          else
            I18n.locale = 'ar'
          end      
      end
end
  
  