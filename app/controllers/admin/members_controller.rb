class Admin::MembersController < Admin::AdminController
    before_action :set_member, only: [:show, :edit, :update, :destroy]
  
  
  
  # GET /users
  # GET /users.json
  def index
    if current_user.role_id == "admin"
      @users = Member.where("role_id =? ", 0).all
    else
      redirect_to root_path, notice: 'Not allowed.' 
    end
  end
  
  def user_search
    @users = Member.where("role_id =? ",0).all
    @users = @users.where(email: params[:emailsearch]) if  params[:emailsearch].present?
    @users = @users.where(["username LIKE ?","%#{params[:usernamesearch]}%"]) if  params[:usernamesearch].present?
    @users = @users.where(phone: params[:phonesearch]) if  params[:phonesearch].present?
    @users = @users.where(active: params[:active]) if  params[:active].present?
  end
  
  def admins
    if current_user.role_id == "admin"
      @admins = Member.where("role_id =? ", 1).all
    else
      redirect_to root_path, notice: 'Not allowed.' 
    end
  end
  
  def admin_search
    @admins = Member.where("role_id =? ",1).all
    @admins = @admins.where(email: params[:emailsearch]) if  params[:emailsearch].present?
    @admins = @admins.where(["username LIKE ?","%#{params[:usernamesearch]}%"]) if  params[:usernamesearch].present?
    @admins = @admins.where(phone: params[:phonesearch]) if  params[:phonesearch].present?
    @admins = @admins.where(active: params[:active]) if  params[:active].present?
  end
  
  # GET /users/1
  # GET /users/1.json
  def show
    unless (current_user.role_id == "admin" or current_user.id == @user.id)
      redirect_to root_path, notice: 'Not allowed.' 
    end
  end
  
  # GET /users/new
  def new
    @user = Member.new
  end
  
  def add_user
    if (current_user.role_id == "admin")
      if request.method == 'GET'
        @user = Member.new
      elsif request.method == 'POST'
        @password = SecureRandom.hex(4)
        @user = Member.new(member_params)
        @user.password = @password
        respond_to do |format|
          if @user.save
            format.html { redirect_to admin_members_path	, notice: 'user was successfully created' }
            format.json { render :show, status: :created, location: @user }
            UserMailer.send_signin_data(params[:member][:email],@password).deliver_later
          else
            format.html { render :add_user }
            format.json { render json: @user.errors, status: :unprocessable_entity }
          end
        end
      end
    else
      redirect_to root_path, notice: 'Not allowed.'
    end
  end
  
  # GET /users/1/edit
  def edit
    unless (current_user.role_id == "admin" or current_user.id == @user.id)
      redirect_to root_path, notice: 'Not allowed.' 
    end
  end
  
  def edit_password
    @user = Member.find(current_user.id)
  end
  
  # POST /users
  # POST /users.json
  def create
    @user = Member.new(member_params)
  
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    unless (current_user.role_id == "admin" or current_user.id == @user.id)
      redirect_to root_path, notice: 'Not allowed.' 
    else
      if (params[:member][:confirm_password])
        @user = Member.authenticate(current_user.email,params[:member][:confirm_password])
        if !(@user)
          if (params[:member][:password])
            redirect_to edit_password_path , notice: "wrong password confirmation" and return
          else
            redirect_to edit_member_path(params[:id]) , notice: "wrong password confirmation" and return
          end
        end
      end
      @current_account_status = @user.active  
      if @user.update(member_params)
        if ( @current_account_status == "Disabled" and params[:member][:active] == "Active" )
          UserMailer.activate_account(@user.email,@user.username).deliver_later
        end
        redirect_to(request.env['HTTP_REFERER'],notice:t("Data was successfully updated"))
      else
        render :edit
      end
    end
  end
  
  # after user sign in, he redirected to this page to confirm signin.
  # user should enter the code which has been sent to his email or his google code.
  # to check the correctness of the entered code.
  # @param [String] code
  # @param [String] token_code
  def confirm_signin
    if request.method == 'GET'
      session[:verify_code] = false
      #render :layout => false
    elsif request.method == 'POST'
      @user = current_user 
      session[:verify_code] = false
      @user_code = params[:confirm][:code]
      @sent_code = session[:token_code]
      if (@user_code == @sent_code) 
        session[:verify_code] = true
        redirect_to @user, notice: "Welcome back!"
      else 
        redirect_to confirm_signin_path, notice: "wrong code!" 
      end 
    end    
  end
  
  # after number of failed login attempts, user's account will be disabled and link will be send to user's mail. 
  # when the user click the link, he is redirected here to unlock account and allow him to signin again. 
  # @param [Integer] user_id 
  # @param [String] token 
  def unlockaccount
    @id = params[:id]
    @token = params[:token]
    @user = Member.where("id =?",@id ).first
    if @token  == @user.unlock_token      
        @user.failed_attempts = 0
        @user.locked = 0
        @user.unlock_token = ""
        if @user.save
        flash.now.notice = 'Your account is Unlocked,please sign in .'
        end
        render template: "sessions/new"  
    else
      flash.now.notice = 'InValid Token .'
      render template: "sessions/new"  
    end
  end
  
  
  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_members_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  
  
  
  
  private
  # Use callbacks to share common setup or constraints between actions.
  def set_member
      @user = Member.find(params[:id])
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def member_params
      params.require(:member).permit(:username, :email, :password, :phone, :address, :role_id, :active, :locked, :sign_in_count, :failed_attempts, :unlock_token, :company_type, :company_activity, :fax, :admin_name, :admin_job)
  end
  
  
  end