class Admin::RequiredFilesController < Admin::AdminController
  before_action :set_required_file, only: [:show, :edit, :update, :destroy]

  # GET /required_files
  # GET /required_files.json
  def index
    @required_files = RequiredFile.all.includes(:user)
    respond_to do |format|
      format.html
      format.csv { send_data @required_files.to_csv }
      format.xls { send_data @required_files.to_csv(col_sep: "\t") }
    end
  end

  # GET /required_files/1
  # GET /required_files/1.json
  def show
  end

  # GET /required_files/new
  def new
    @required_file = RequiredFile.new
  end

  # GET /required_files/1/edit
  def edit
  end

  # POST /required_files
  # POST /required_files.json
  def create
    @required_file = RequiredFile.new(required_file_params)
    @required_file.status == "created"

    respond_to do |format|
      if @required_file.save
        format.html { redirect_to admin_required_file_path(@required_file), notice: 'Required file was successfully created.' }
        format.json { render :show, status: :created, location: @required_file }
      else
        format.html { render :new }
        format.json { render json: @required_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /required_files/1
  # PATCH/PUT /required_files/1.json
  def update
    respond_to do |format|
      if @required_file.update(required_file_params)
        format.html { redirect_to admin_required_file_path(@required_file), notice: 'Required file was successfully updated.' }
        format.json { render :show, status: :ok, location: @required_file }
      else
        format.html { render :edit }
        format.json { render json: @required_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /required_files/1
  # DELETE /required_files/1.json
  def destroy
    @required_file.destroy
    respond_to do |format|
      format.html { redirect_to admin_required_files_url, notice: 'Required file was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_required_file
      @required_file = RequiredFile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def required_file_params
      params.require(:required_file).permit(:user_id, :description, :status, :file)
    end
end
