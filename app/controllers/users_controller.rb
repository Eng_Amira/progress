class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :require_login , except: [:new, :create, :unlockaccount]
  skip_before_action :check2fa ,only: [:new, :destroy, :create, :unlockaccount]
  layout 'user'


  # GET /users/1
  # GET /users/1.json
  def show
    unless (current_user.role_id == "admin" or current_user.id == @user.id)
      redirect_to root_path, notice: 'Not allowed.' 
    end
  end

  # GET /users/new
  def new
    @user = User.new
    render layout: false
  end


  # GET /users/1/edit
  def edit
    unless (current_user.id == @user.id)
      redirect_to root_path, notice: 'Not allowed.' 
    end
  end

  def edit_password
    @user = User.find(current_user.id)
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.active = "Disabled"
    @user.role_id = "user"

    #respond_to do |format|
      if @user.save
        UserMailer.new_account(@user.email,@user.username).deliver_later
        AuditLog.create(user_id: @user.id, added_by: @user.id, foreign_id: @user.id, action_type: "create account", meta_data: "New User has been registerd", ip_address: request.remote_ip, user_agent: request.user_agent)
        redirect_to sign_in_path, notice: t('account_created_waiting_admin_confirm')
        #format.html { redirect_to @user, notice: 'User was successfully created.' }
        #format.json { render :show, status: :created, location: @user }
      else
        render :new, layout: false, notice: @user.errors 
        #redirect_to sign_up_path, notice: @user.errors
        #format.html { render :new, layout: false, notice: @user.errors }
        #format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    #end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    unless (current_user.id == @user.id)
      redirect_to root_path, notice: 'Not allowed.' 
    else
      @user = User.authenticate(current_user.email,params[:user][:confirm_password])
        if !(@user)
          if (params[:user][:password])
            redirect_to edit_password_path , notice: t("wrong password confirmation") and return
          else
            redirect_to edit_user_path(params[:id]) , notice: t("wrong password confirmation") and return
          end
        else
          respond_to do |format|
            if @user.update(user_params)
              format.html { redirect_to @user, notice: t('Data was successfully updated') }
              format.json { render :show, status: :ok, location: @user }
            else
              format.html { render :edit }
              format.json { render json: @user.errors, status: :unprocessable_entity }
            end
          end
        end
    end
  end

  # after user sign in, he redirected to this page to confirm signin.
  # user should enter the code which has been sent to his email or his google code.
  # to check the correctness of the entered code.
  # @param [String] code
  # @param [String] token_code
  def confirm_signin
    if request.method == 'GET'
      render :layout => false
      session[:verify_code] = false
      #render :layout => false
    elsif request.method == 'POST'
      @user = current_user 
      session[:verify_code] = false
      @user_code = params[:confirm][:code]
      @sent_code = session[:token_code]
      if (@user_code == @sent_code) 
        session[:verify_code] = true
        if @user.role_id == "admin"
          redirect_to admin_dashboard_path, notice: "Welcome back!"
        else
          redirect_to @user, notice: "Welcome back!"
        end
      else 
        redirect_to confirm_signin_path, notice: "wrong code!" 
      end 
    end    
  end


  def resend_confirmation_code
    if (session[:token_code] != nil and session[:token_code] != "")
      @token = session[:token_code]
    else
      @token = SecureRandom.hex(4)
    end
    UserMailer.confirmsignin(current_user.email,current_user.username,@token).deliver_later
    redirect_to confirm_signin_path, notice: t("code_resended")
  end

  # after number of failed login attempts, user's account will be disabled and link will be send to user's mail. 
  # when the user click the link, he is redirected here to unlock account and allow him to signin again. 
  # @param [Integer] user_id 
  # @param [String] token 
  def unlockaccount
    @id = params[:id]
    @token = params[:token]
    @user = User.where("id =?",@id ).first
    if @token  == @user.unlock_token      
       @user.failed_attempts = 0
       @user.locked = 0
       @user.unlock_token = ""
       if @user.save
        flash.now.notice = t('your_account_unlocked')
       end
       render template: "sessions/new",  layout: false
    else
      flash.now.notice = 'InValid Token .'
      render template: "sessions/new" ,layout: false 
    end
  end


  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to root_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def models
    @models = Model.where("status =? ", true).all
  end

  def reports
    @reports = Report.where("user_id =? ", current_user.id).all
  end


  def required_files
    @required_files = RequiredFile.where("user_id =? ", current_user.id).all
  end

  def update_file
    @required_files = RequiredFile.where("id =? ", params[:required_file][:id]).first
    if @required_files
      if @required_files.update(:status => "pending", :file => params[:required_file][:file])
        AuditLog.create(user_id: current_user.id, added_by: current_user.id, foreign_id: @required_files.id, action_type: "Edit required file", meta_data: "Required file was updated", ip_address: request.remote_ip, user_agent: request.user_agent)
        redirect_to required_files_path, notice: 'Required file was successfully updated.'
      else
        render :required_files 
      end
    end
  end

  def notifications
    @notifications = Notification.where("user_id =? ", current_user.id).all
  end


  def show_notification
    @notification = Notification.where("id =? ", params[:id]).first
    @notification.update(seen: true)

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :email, :password, :phone, :address, :role_id, :active, :locked, :sign_in_count, :failed_attempts, :unlock_token, :company_type, :company_activity, :fax, :admin_name, :admin_job)
    end
end
