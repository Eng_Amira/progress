class VisitorServicesController < ApplicationController
  before_action :set_visitor_service, only: [:show, :edit, :update, :destroy]
  before_action :require_login , except: [:new, :create, :send_email]
  layout 'site'



  # GET /visitor_services/new
  def new
    @visitor_service = VisitorService.new
  end

  # POST /visitor_services
  # POST /visitor_services.json
  def create
    @visitor_service = VisitorService.new(visitor_service_params)

    respond_to do |format|
      if @visitor_service.save
        UserMailer.newvisitor_request(@visitor_service.email,@visitor_service.name,@visitor_service.code).deliver_later
        format.html { redirect_to pages_visitor_login_path, notice: t('Visitor service was successfully created with number') + ' : ' +  @visitor_service.code}
        format.json { render :show, status: :created, location: @visitor_service }
      else
        format.html { render :new }
        format.json { render json: @visitor_service.errors, status: :unprocessable_entity }
      end
    end
  end

  def send_email
    @email = params[:email]
    @name = params[:name]
    @msg = params[:msg]
    @send_to = Content.where(:category => 5, :key => "contacts").first.title_en.to_s
    UserMailer.sendmessage(@email,@name,@msg,@send_to).deliver_later
    redirect_back fallback_location: pages_contact_us_path, notice: "تم ارسال الرسالة ، سيتم الرد عليها بريديا من طرف الادارة"
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_visitor_service
      @visitor_service = VisitorService.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def visitor_service_params
      params.require(:visitor_service).permit(:service_id, :name, :company_type, :email, :phone, :address, :birth_date, :job_title, :adminstrator_name, :previous_work, :details, :code, :status, :reply, :file)
    end
end
