class Clearance::SessionsController < Clearance::BaseController
  if respond_to?(:before_action)
    before_action :redirect_signed_in_users, only: [:new]
    skip_before_action :require_login,
      only: [:create, :new, :destroy],
      raise: false
    skip_before_action :authorize,
      only: [:create, :new, :destroy],
      raise: false
  else
    before_filter :redirect_signed_in_users, only: [:new]
    skip_before_filter :require_login,
      only: [:create, :new, :destroy],
      raise: false
    skip_before_filter :authorize,
      only: [:create, :new, :destroy],
      raise: false
  end
  skip_before_action :check2fa ,only: [:new, :destroy, :create]


  def create
    @user = authenticate(params)
    @token = SecureRandom.hex(4)
    sign_in(@user) do |status|
      if status.success?
        session[:token_code] = @token
        @user.sign_in_count += 1
        @user.failed_attempts = 0
        @user.save
        UserMailer.confirmsignin(current_user.email,current_user.username,@token).deliver_later
        redirect_to confirm_signin_path
        #redirect_back_or url_after_create
      else
        @currentuser = User.where("email =?",params[:session][:email].to_s).first
        if @currentuser != nil 
          @currentuser.failed_attempts += 1
          if @currentuser.failed_attempts >= 3
            @currentuser.locked = 1
            if (@currentuser.unlock_token == nil or @currentuser.unlock_token == "")
              @currentuser.unlock_token = @token
            end
            #UserMailer.unlockaccount(@currentuser,@token).deliver_later
          end
          @currentuser.save
        end
          #flash.now.notice = status.failure_message 
          redirect_to sign_in_path, notice: status.failure_message
          #render template: "sessions/new", status: :unauthorized        
   
      end
    end
  end

  def destroy
    sign_out
    redirect_to url_after_destroy
  end

  def new
    render template: "sessions/new",layout: false
  end

  private

  def redirect_signed_in_users
    if signed_in?      
      if current_user.role_id == "admin"
        redirect_to admin_dashboard_path
      elsif current_user.role_id == "user"
        redirect_to pages_home_path
      end
      #redirect_to url_for_signed_in_users
    end
  end

  def url_after_create
    Clearance.configuration.redirect_url
  end

  def url_after_destroy
    sign_in_url
  end

  def url_for_signed_in_users
    url_after_create
  end
end
