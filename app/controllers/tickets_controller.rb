class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]
  before_action :require_login
  layout 'user'

  # GET /tickets
  # GET /tickets.json
  def index
    @tickets = Ticket.where(:user_id => current_user.id, :ticket_id => nil).all
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
    @ticket_replies = Ticket.where(ticket_id:  params[:id]).includes(:user).all.order("created_at desc")
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new
  end

  # GET /tickets/1/edit
  def edit
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Ticket.new(ticket_params)
    @ticket.ticket_number =  Random.new.rand(111111..999999)
    @ticket.user_id = current_user.id
    @ticket.status = "opened"
    @ticket_id = params[:ticket][:ticket_id]
    if params[:ticket][:ticket_type] == nil
      @ticket.ticket_type = "created_by_user"
    end

    respond_to do |format|
      if @ticket.save
        if @ticket_id == nil
          AuditLog.create(user_id: current_user.id, added_by: current_user.id, foreign_id: @ticket.id, action_type: "create new ticket", meta_data: "User has created new ticket", ip_address: request.remote_ip, user_agent: request.user_agent)
          format.html { redirect_to @ticket, notice: 'Ticket was successfully created.' }
        else
          AuditLog.create(user_id: current_user.id, added_by: current_user.id, foreign_id: @ticket_id, action_type: "reply to ticket", meta_data: "User has replied to ticket", ip_address: request.remote_ip, user_agent: request.user_agent)
          format.html { redirect_to ticket_path(@ticket_id), notice: 'Reply was successfully created.' }
        end
        format.json { render :show, status: :created, location: @ticket }
      else
        format.html { render :new }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    respond_to do |format|
      if @ticket.update(ticket_params)
        format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.html { render :edit }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: 'Ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:user_id, :ticket_id, :ticket_number, :title, :description, :status, :ticket_type)
    end
end
