class PremiumCustomer < ApplicationRecord
    has_one_attached :image
    enum category: { customer_opinions: 0, our_partners: 1 }
    validate :attachment_type


    def attachment_type
        if image.attached? 
            if !image.attachment.blob.content_type.in?(%w(image/png image/jpg image/jpeg image/tiff image/tif))
                errors.add(:image, I18n.t('not_allowed_file_type'))
            elsif image.blob.byte_size > (2097152) 
                errors.add(:image, I18n.t('max_size_2MB'))
            end
        end
    end

end
