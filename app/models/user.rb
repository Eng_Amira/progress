class User < ApplicationRecord
  include Clearance::User
  has_many :required_file , foreign_key: "user_id"
  has_many :report , foreign_key: "user_id"
  has_many :notification , foreign_key: "user_id"
  has_many :ticket , foreign_key: "user_id"
  has_many :basic_datum , foreign_key: "user_id"
  has_many :audit_log , foreign_key: "user_id"

  enum role_id: { user: 0, admin: 1 }
  enum active: { Active: 1, Disabled: 0}
  enum company_type: { Business_Administration: 1, Accounting: 2 }

  validates :email, uniqueness: true, presence: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
  validates :password, length: 8..20, presence: true, if: :should_validate_password?
  validates :username, length: { maximum: 30 }, presence: true
  validates :address, :company_type, :company_activity, :admin_name, :admin_job, :phone, :fax, length: { maximum: 100 }

  validates_inclusion_of :role_id, :in => ['user', 'admin'], :allow_nil => false
  validates_inclusion_of :active, :in => ['Active', 'Disabled'], :allow_nil => false

 



  def should_validate_password?
    password.present?
  end

  


end
