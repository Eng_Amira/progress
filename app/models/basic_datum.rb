class BasicDatum < ApplicationRecord
    has_one_attached :file
    belongs_to :model, foreign_key: "model_id"
    belongs_to :user, foreign_key: "user_id"
    enum status: { pending: 0, accepted: 1 }

end
