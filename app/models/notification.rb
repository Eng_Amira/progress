class Notification < ApplicationRecord
    enum seen: { seen: true, not_seen: false }
    belongs_to :user ,:foreign_key => "user_id"
end
