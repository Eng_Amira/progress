class RequiredFile < ApplicationRecord
    has_one_attached :file
    enum status: { created: 0, pending: 1, accepted: 2, rejected: 3 }
    belongs_to :user ,:foreign_key => "user_id"

    

    def self.to_csv(options = {})
      attributes = %w{id name description status created_at}    
        CSV.generate(headers: true) do |csv|
          csv << attributes    
          all.each do |required_files|
            csv << [required_files.id, required_files.user.username, required_files.description, required_files.status, required_files.created_at]
          end
        end
    end

end
