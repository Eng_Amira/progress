class Ticket < ApplicationRecord
    enum status: { opened: 0, closed: 1, Received: 2}
    enum ticket_type: { created_by_user: 0, created_by_admin: 1, admin_reply: 2, user_reply: 3 }

    belongs_to :user ,:foreign_key => "user_id"

end
