class New < ApplicationRecord
    has_many_attached :images
    validate :attachment_type

    def attachment_type
        if images.attached?
            images.each do |image|
                if !image.blob.content_type.in?(%w(image/jpeg image/jpg image/png image/tiff image/tif))
                    errors.add(:images, I18n.t('not_allowed_file_type'))
                elsif image.blob.byte_size > (2097152)
                    errors.add(:images, I18n.t('max_size_2MB'))
                end
            end
        end
    end 

end
