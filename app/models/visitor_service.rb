class VisitorService < ApplicationRecord
  has_one_attached :file

    enum service_id: { ServiceRequest: 1, ConsultRequest: 2, JobRequest: 3, Suggestion:4 }
    enum status: { Pending: 0, Completed: 1, Rejected: 2 }
    validates_uniqueness_of :code
    validate :attachment_type

    before_save :generate_code

    def generate_code
      begin
        self.code ||= [*('0'..'9')].to_a.shuffle[0,6].join
      end until valid?
    end

    def attachment_type
      if file.attached? 
        if !file.attachment.blob.content_type.in?(%w(image/png image/jpg image/jpeg image/tiff image/tif application/pdf application/docx application/doc application/xls))
          errors.add(:file, I18n.t('not_allowed_file_type'))
        elsif file.blob.byte_size > (2097152) && file.blob.content_type.in?(%w(image/jpeg image/jpg image/png image/tiff image/tif application/pdf))
          errors.add(:file, I18n.t('max_size_2MB'))
        end
      end
    end

    def self.to_csv(options = {})
      CSV.generate(options) do |csv|
        csv << column_names
        all.each do |visitor_service|
          csv << visitor_service.attributes.values_at(*column_names)
        end
      end
    end

    #def self.to_csv(options = {})
     # desired_columns = ["id", "name"]
      #CSV.generate(options) do |csv|
       # csv << desired_columns
        #all.each do |visitor_service|
         # csv << visitor_service.attributes.values_at(*desired_columns)
        #end
      #end
    #end

end
