class ConfirmationGuard < Clearance::SignInGuard
  def call
    if signed_in? && current_user.active == 1
      success
    else
      failure("You must confirm your email address.")
    end
  end
end