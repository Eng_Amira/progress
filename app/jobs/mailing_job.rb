class MailingJob 

    include SuckerPunch::Job

    # Email Provider Background Job
    def perform(params)
      UserMailer.admin_reply(params[:name],params[:email],params[:code],params[:subject],params[:reply]).deliver_later
    end
    

end