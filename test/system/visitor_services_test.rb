require "application_system_test_case"

class VisitorServicesTest < ApplicationSystemTestCase
  setup do
    @visitor_service = visitor_services(:one)
  end

  test "visiting the index" do
    visit visitor_services_url
    assert_selector "h1", text: "Visitor Services"
  end

  test "creating a Visitor service" do
    visit visitor_services_url
    click_on "New Visitor Service"

    fill_in "Address", with: @visitor_service.address
    fill_in "Adminstrator name", with: @visitor_service.adminstrator_name
    fill_in "Birth date", with: @visitor_service.birth_date
    fill_in "Company type", with: @visitor_service.company_type
    fill_in "Details", with: @visitor_service.details
    fill_in "Email", with: @visitor_service.email
    fill_in "Job title", with: @visitor_service.job_title
    fill_in "Name", with: @visitor_service.name
    fill_in "Phone", with: @visitor_service.phone
    fill_in "Previous work", with: @visitor_service.previous_work
    fill_in "Service", with: @visitor_service.service_id
    click_on "Create Visitor service"

    assert_text "Visitor service was successfully created"
    click_on "Back"
  end

  test "updating a Visitor service" do
    visit visitor_services_url
    click_on "Edit", match: :first

    fill_in "Address", with: @visitor_service.address
    fill_in "Adminstrator name", with: @visitor_service.adminstrator_name
    fill_in "Birth date", with: @visitor_service.birth_date
    fill_in "Company type", with: @visitor_service.company_type
    fill_in "Details", with: @visitor_service.details
    fill_in "Email", with: @visitor_service.email
    fill_in "Job title", with: @visitor_service.job_title
    fill_in "Name", with: @visitor_service.name
    fill_in "Phone", with: @visitor_service.phone
    fill_in "Previous work", with: @visitor_service.previous_work
    fill_in "Service", with: @visitor_service.service_id
    click_on "Update Visitor service"

    assert_text "Visitor service was successfully updated"
    click_on "Back"
  end

  test "destroying a Visitor service" do
    visit visitor_services_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Visitor service was successfully destroyed"
  end
end
