require "application_system_test_case"

class PremiumCustomersTest < ApplicationSystemTestCase
  setup do
    @premium_customer = premium_customers(:one)
  end

  test "visiting the index" do
    visit premium_customers_url
    assert_selector "h1", text: "Premium Customers"
  end

  test "creating a Premium customer" do
    visit premium_customers_url
    click_on "New Premium Customer"

    fill_in "Details", with: @premium_customer.details
    fill_in "Key", with: @premium_customer.key
    fill_in "Name", with: @premium_customer.name
    fill_in "Status", with: @premium_customer.status
    click_on "Create Premium customer"

    assert_text "Premium customer was successfully created"
    click_on "Back"
  end

  test "updating a Premium customer" do
    visit premium_customers_url
    click_on "Edit", match: :first

    fill_in "Details", with: @premium_customer.details
    fill_in "Key", with: @premium_customer.key
    fill_in "Name", with: @premium_customer.name
    fill_in "Status", with: @premium_customer.status
    click_on "Update Premium customer"

    assert_text "Premium customer was successfully updated"
    click_on "Back"
  end

  test "destroying a Premium customer" do
    visit premium_customers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Premium customer was successfully destroyed"
  end
end
