require "application_system_test_case"

class RequiredFilesTest < ApplicationSystemTestCase
  setup do
    @required_file = required_files(:one)
  end

  test "visiting the index" do
    visit required_files_url
    assert_selector "h1", text: "Required Files"
  end

  test "creating a Required file" do
    visit required_files_url
    click_on "New Required File"

    fill_in "Description", with: @required_file.description
    fill_in "User", with: @required_file.user_id
    click_on "Create Required file"

    assert_text "Required file was successfully created"
    click_on "Back"
  end

  test "updating a Required file" do
    visit required_files_url
    click_on "Edit", match: :first

    fill_in "Description", with: @required_file.description
    fill_in "User", with: @required_file.user_id
    click_on "Update Required file"

    assert_text "Required file was successfully updated"
    click_on "Back"
  end

  test "destroying a Required file" do
    visit required_files_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Required file was successfully destroyed"
  end
end
