require "application_system_test_case"

class BasicDataTest < ApplicationSystemTestCase
  setup do
    @basic_datum = basic_data(:one)
  end

  test "visiting the index" do
    visit basic_data_url
    assert_selector "h1", text: "Basic Data"
  end

  test "creating a Basic datum" do
    visit basic_data_url
    click_on "New Basic Datum"

    fill_in "Model", with: @basic_datum.model_id
    fill_in "User", with: @basic_datum.user_id
    click_on "Create Basic datum"

    assert_text "Basic datum was successfully created"
    click_on "Back"
  end

  test "updating a Basic datum" do
    visit basic_data_url
    click_on "Edit", match: :first

    fill_in "Model", with: @basic_datum.model_id
    fill_in "User", with: @basic_datum.user_id
    click_on "Update Basic datum"

    assert_text "Basic datum was successfully updated"
    click_on "Back"
  end

  test "destroying a Basic datum" do
    visit basic_data_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Basic datum was successfully destroyed"
  end
end
