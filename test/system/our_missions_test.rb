require "application_system_test_case"

class OurMissionsTest < ApplicationSystemTestCase
  setup do
    @our_mission = our_missions(:one)
  end

  test "visiting the index" do
    visit our_missions_url
    assert_selector "h1", text: "Our Missions"
  end

  test "creating a Our mission" do
    visit our_missions_url
    click_on "New Our Mission"

    fill_in "Description", with: @our_mission.description
    fill_in "Title", with: @our_mission.title
    click_on "Create Our mission"

    assert_text "Our mission was successfully created"
    click_on "Back"
  end

  test "updating a Our mission" do
    visit our_missions_url
    click_on "Edit", match: :first

    fill_in "Description", with: @our_mission.description
    fill_in "Title", with: @our_mission.title
    click_on "Update Our mission"

    assert_text "Our mission was successfully updated"
    click_on "Back"
  end

  test "destroying a Our mission" do
    visit our_missions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Our mission was successfully destroyed"
  end
end
