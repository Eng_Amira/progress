require "application_system_test_case"

class ErrorsLogsTest < ApplicationSystemTestCase
  setup do
    @errors_log = errors_logs(:one)
  end

  test "visiting the index" do
    visit errors_logs_url
    assert_selector "h1", text: "Errors Logs"
  end

  test "creating a Errors log" do
    visit errors_logs_url
    click_on "New Errors Log"

    fill_in "Code", with: @errors_log.code
    fill_in "Location", with: @errors_log.location
    fill_in "Message", with: @errors_log.message
    fill_in "User", with: @errors_log.user_id
    click_on "Create Errors log"

    assert_text "Errors log was successfully created"
    click_on "Back"
  end

  test "updating a Errors log" do
    visit errors_logs_url
    click_on "Edit", match: :first

    fill_in "Code", with: @errors_log.code
    fill_in "Location", with: @errors_log.location
    fill_in "Message", with: @errors_log.message
    fill_in "User", with: @errors_log.user_id
    click_on "Update Errors log"

    assert_text "Errors log was successfully updated"
    click_on "Back"
  end

  test "destroying a Errors log" do
    visit errors_logs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Errors log was successfully destroyed"
  end
end
