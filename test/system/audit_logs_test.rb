require "application_system_test_case"

class AuditLogsTest < ApplicationSystemTestCase
  setup do
    @audit_log = audit_logs(:one)
  end

  test "visiting the index" do
    visit audit_logs_url
    assert_selector "h1", text: "Audit Logs"
  end

  test "creating a Audit log" do
    visit audit_logs_url
    click_on "New Audit Log"

    fill_in "Action type", with: @audit_log.action_type
    fill_in "Added by", with: @audit_log.added_by
    fill_in "Foreign", with: @audit_log.foreign_id
    fill_in "Ip address", with: @audit_log.ip_address
    fill_in "Meta data", with: @audit_log.meta_data
    fill_in "User agent", with: @audit_log.user_agent
    fill_in "User", with: @audit_log.user_id
    click_on "Create Audit log"

    assert_text "Audit log was successfully created"
    click_on "Back"
  end

  test "updating a Audit log" do
    visit audit_logs_url
    click_on "Edit", match: :first

    fill_in "Action type", with: @audit_log.action_type
    fill_in "Added by", with: @audit_log.added_by
    fill_in "Foreign", with: @audit_log.foreign_id
    fill_in "Ip address", with: @audit_log.ip_address
    fill_in "Meta data", with: @audit_log.meta_data
    fill_in "User agent", with: @audit_log.user_agent
    fill_in "User", with: @audit_log.user_id
    click_on "Update Audit log"

    assert_text "Audit log was successfully updated"
    click_on "Back"
  end

  test "destroying a Audit log" do
    visit audit_logs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Audit log was successfully destroyed"
  end
end
