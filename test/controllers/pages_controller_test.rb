require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get homepage" do
    get pages_homepage_url
    assert_response :success
  end

  test "should get visitor_login" do
    get pages_visitor_login_url
    assert_response :success
  end

end
