require 'test_helper'

class VisitorServicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @visitor_service = visitor_services(:one)
  end

  test "should get index" do
    get visitor_services_url
    assert_response :success
  end

  test "should get new" do
    get new_visitor_service_url
    assert_response :success
  end

  test "should create visitor_service" do
    assert_difference('VisitorService.count') do
      post visitor_services_url, params: { visitor_service: { address: @visitor_service.address, adminstrator_name: @visitor_service.adminstrator_name, birth_date: @visitor_service.birth_date, company_type: @visitor_service.company_type, details: @visitor_service.details, email: @visitor_service.email, job_title: @visitor_service.job_title, name: @visitor_service.name, phone: @visitor_service.phone, previous_work: @visitor_service.previous_work, service_id: @visitor_service.service_id } }
    end

    assert_redirected_to visitor_service_url(VisitorService.last)
  end

  test "should show visitor_service" do
    get visitor_service_url(@visitor_service)
    assert_response :success
  end

  test "should get edit" do
    get edit_visitor_service_url(@visitor_service)
    assert_response :success
  end

  test "should update visitor_service" do
    patch visitor_service_url(@visitor_service), params: { visitor_service: { address: @visitor_service.address, adminstrator_name: @visitor_service.adminstrator_name, birth_date: @visitor_service.birth_date, company_type: @visitor_service.company_type, details: @visitor_service.details, email: @visitor_service.email, job_title: @visitor_service.job_title, name: @visitor_service.name, phone: @visitor_service.phone, previous_work: @visitor_service.previous_work, service_id: @visitor_service.service_id } }
    assert_redirected_to visitor_service_url(@visitor_service)
  end

  test "should destroy visitor_service" do
    assert_difference('VisitorService.count', -1) do
      delete visitor_service_url(@visitor_service)
    end

    assert_redirected_to visitor_services_url
  end
end
