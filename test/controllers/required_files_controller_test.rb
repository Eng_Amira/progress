require 'test_helper'

class RequiredFilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @required_file = required_files(:one)
  end

  test "should get index" do
    get required_files_url
    assert_response :success
  end

  test "should get new" do
    get new_required_file_url
    assert_response :success
  end

  test "should create required_file" do
    assert_difference('RequiredFile.count') do
      post required_files_url, params: { required_file: { description: @required_file.description, user_id: @required_file.user_id } }
    end

    assert_redirected_to required_file_url(RequiredFile.last)
  end

  test "should show required_file" do
    get required_file_url(@required_file)
    assert_response :success
  end

  test "should get edit" do
    get edit_required_file_url(@required_file)
    assert_response :success
  end

  test "should update required_file" do
    patch required_file_url(@required_file), params: { required_file: { description: @required_file.description, user_id: @required_file.user_id } }
    assert_redirected_to required_file_url(@required_file)
  end

  test "should destroy required_file" do
    assert_difference('RequiredFile.count', -1) do
      delete required_file_url(@required_file)
    end

    assert_redirected_to required_files_url
  end
end
