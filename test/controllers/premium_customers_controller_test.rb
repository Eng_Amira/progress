require 'test_helper'

class PremiumCustomersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @premium_customer = premium_customers(:one)
  end

  test "should get index" do
    get premium_customers_url
    assert_response :success
  end

  test "should get new" do
    get new_premium_customer_url
    assert_response :success
  end

  test "should create premium_customer" do
    assert_difference('PremiumCustomer.count') do
      post premium_customers_url, params: { premium_customer: { details: @premium_customer.details, key: @premium_customer.key, name: @premium_customer.name, status: @premium_customer.status } }
    end

    assert_redirected_to premium_customer_url(PremiumCustomer.last)
  end

  test "should show premium_customer" do
    get premium_customer_url(@premium_customer)
    assert_response :success
  end

  test "should get edit" do
    get edit_premium_customer_url(@premium_customer)
    assert_response :success
  end

  test "should update premium_customer" do
    patch premium_customer_url(@premium_customer), params: { premium_customer: { details: @premium_customer.details, key: @premium_customer.key, name: @premium_customer.name, status: @premium_customer.status } }
    assert_redirected_to premium_customer_url(@premium_customer)
  end

  test "should destroy premium_customer" do
    assert_difference('PremiumCustomer.count', -1) do
      delete premium_customer_url(@premium_customer)
    end

    assert_redirected_to premium_customers_url
  end
end
