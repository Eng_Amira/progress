require 'test_helper'

class BasicDataControllerTest < ActionDispatch::IntegrationTest
  setup do
    @basic_datum = basic_data(:one)
  end

  test "should get index" do
    get basic_data_url
    assert_response :success
  end

  test "should get new" do
    get new_basic_datum_url
    assert_response :success
  end

  test "should create basic_datum" do
    assert_difference('BasicDatum.count') do
      post basic_data_url, params: { basic_datum: { model_id: @basic_datum.model_id, user_id: @basic_datum.user_id } }
    end

    assert_redirected_to basic_datum_url(BasicDatum.last)
  end

  test "should show basic_datum" do
    get basic_datum_url(@basic_datum)
    assert_response :success
  end

  test "should get edit" do
    get edit_basic_datum_url(@basic_datum)
    assert_response :success
  end

  test "should update basic_datum" do
    patch basic_datum_url(@basic_datum), params: { basic_datum: { model_id: @basic_datum.model_id, user_id: @basic_datum.user_id } }
    assert_redirected_to basic_datum_url(@basic_datum)
  end

  test "should destroy basic_datum" do
    assert_difference('BasicDatum.count', -1) do
      delete basic_datum_url(@basic_datum)
    end

    assert_redirected_to basic_data_url
  end
end
