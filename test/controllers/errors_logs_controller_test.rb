require 'test_helper'

class ErrorsLogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @errors_log = errors_logs(:one)
  end

  test "should get index" do
    get errors_logs_url
    assert_response :success
  end

  test "should get new" do
    get new_errors_log_url
    assert_response :success
  end

  test "should create errors_log" do
    assert_difference('ErrorsLog.count') do
      post errors_logs_url, params: { errors_log: { code: @errors_log.code, location: @errors_log.location, message: @errors_log.message, user_id: @errors_log.user_id } }
    end

    assert_redirected_to errors_log_url(ErrorsLog.last)
  end

  test "should show errors_log" do
    get errors_log_url(@errors_log)
    assert_response :success
  end

  test "should get edit" do
    get edit_errors_log_url(@errors_log)
    assert_response :success
  end

  test "should update errors_log" do
    patch errors_log_url(@errors_log), params: { errors_log: { code: @errors_log.code, location: @errors_log.location, message: @errors_log.message, user_id: @errors_log.user_id } }
    assert_redirected_to errors_log_url(@errors_log)
  end

  test "should destroy errors_log" do
    assert_difference('ErrorsLog.count', -1) do
      delete errors_log_url(@errors_log)
    end

    assert_redirected_to errors_logs_url
  end
end
