require 'test_helper'

class OurMissionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @our_mission = our_missions(:one)
  end

  test "should get index" do
    get our_missions_url
    assert_response :success
  end

  test "should get new" do
    get new_our_mission_url
    assert_response :success
  end

  test "should create our_mission" do
    assert_difference('OurMission.count') do
      post our_missions_url, params: { our_mission: { description: @our_mission.description, title: @our_mission.title } }
    end

    assert_redirected_to our_mission_url(OurMission.last)
  end

  test "should show our_mission" do
    get our_mission_url(@our_mission)
    assert_response :success
  end

  test "should get edit" do
    get edit_our_mission_url(@our_mission)
    assert_response :success
  end

  test "should update our_mission" do
    patch our_mission_url(@our_mission), params: { our_mission: { description: @our_mission.description, title: @our_mission.title } }
    assert_redirected_to our_mission_url(@our_mission)
  end

  test "should destroy our_mission" do
    assert_difference('OurMission.count', -1) do
      delete our_mission_url(@our_mission)
    end

    assert_redirected_to our_missions_url
  end
end
