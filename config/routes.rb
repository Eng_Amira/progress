Rails.application.routes.draw do
  
  
  resources :errors_logs
  resources :tickets
  resources :basic_data
  
  root 'pages#homepage'
  get 'pages/visitor_login'
  get 'pages/home'
  get 'pages/contact_us'
  get 'pages/mission'
  get 'pages/about_us'
  get 'pages/human_cadres'
  get 'pages/services'
  get 'pages/customers'
  get 'pages/news'
  get 'pages/details'
  get 'pages/our_company'
  get 'pages/disclaimer'
  get 'pages/terms_of_use'
  get 'pages/privacy_policy'
  get 'pages/change_lang'
  get 'pages/error'
  


  
  get "unlockaccount" => "users#unlockaccount"

  resources :visitor_services, only: [:new, :create]
  resources :users, except: [:index,:destroy]
  resources :audit_logs, only: [:create]
 
  get 'edit_password' => "users#edit_password"
  match 'confirm_signin', to: 'users#confirm_signin', via: %i(get post)
  get 'resend_code' => "users#resend_confirmation_code"
  get 'models' => "users#models"
  get 'reports' => "users#reports"
  get 'required_files' => "users#required_files"
  patch 'update_file' => "users#update_file"
  get 'notifications' => "users#notifications"
  get 'show_notification' => "users#show_notification"
  post 'send_email' => "visitor_services#send_email"



  


  #scope path: 'admin', module: 'admin' do
  namespace :admin do
    match 'add-user', to: 'members#add_user', via: %i(get post)
    get 'admins' => "members#admins"
    get 'users/search' => "members#user_search"
    get 'admins/search' => "members#admin_search"
    get 'visitor_services/search' => "visitor_services#search"
    get 'messages/search' => "messages#ticket_search"
    get 'dashboard' => "pages#dashboard"
    #get 'news' => "pages#news"

    get 'delete_image' => "contents#delete_image"
    get 'delete_news_image' => "news#delete_news_image"
    get 'delete_mission_image' => "our_missions#delete_mission_image"
    get 'delete_customer_image' => "premium_customers#delete_customer_image"





    #resources :users
    resources :audit_logs, only: [:index, :show]
    resources :visitor_services
    resources :contents, except: [:new, :create, :destroy]
    resources :premium_customers
    resources :our_missions, except: [:new, :create, :destroy]
    resources :news
    resources :models
    resources :required_files
    resources :users_basic_data
    resources :reports
    resources :members
    resources :messages
    resources :notifications






  end
  

  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
