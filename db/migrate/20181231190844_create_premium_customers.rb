class CreatePremiumCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :premium_customers do |t|
      t.string :key
      t.string :name
      t.string :details
      t.boolean :status, default: true

      t.timestamps
    end
  end
end
