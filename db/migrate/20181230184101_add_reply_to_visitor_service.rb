class AddReplyToVisitorService < ActiveRecord::Migration[5.2]
  def change
    add_column :visitor_services, :reply, :text
    change_column :visitor_services, :details, :text
  end
end
