class AddColumnToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :company_type, :integer
    add_column :users, :company_activity, :string
    add_column :users, :fax, :string
    add_column :users, :admin_name, :string
    add_column :users, :admin_job, :string

  end
end
