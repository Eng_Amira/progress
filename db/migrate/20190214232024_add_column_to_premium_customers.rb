class AddColumnToPremiumCustomers < ActiveRecord::Migration[5.2]
  def change
    add_column :premium_customers, :category, :integer, :default => 1
  end
end
