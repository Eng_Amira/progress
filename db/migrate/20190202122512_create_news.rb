class CreateNews < ActiveRecord::Migration[5.2]
  def change
    create_table :news do |t|
      t.string :title_en
      t.string :title_ar
      t.text :description_en
      t.text :description_ar
      t.boolean :status, :default => true

      t.timestamps
    end
  end
end
