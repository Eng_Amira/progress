class AddColumnToMission < ActiveRecord::Migration[5.2]
  def change
    add_column :our_missions, :title_en, :string
    add_column :our_missions, :description_en, :text
    add_column :our_missions, :small_pic, :string
    rename_column :our_missions, :title, :title_ar
    rename_column :our_missions, :description, :description_ar
  end
end
