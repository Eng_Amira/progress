class CreateContents < ActiveRecord::Migration[5.2]
  def change
    create_table :contents do |t|
      t.string :key
      t.integer :category
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
