class AddColumnsToUser < ActiveRecord::Migration[5.2]
  def change    
    add_column :users, :role_id, :integer, null: false, default: 0
    add_column :users, :active, :integer, null: false, default: 0
    add_column :users, :locked, :integer, null: false, default: 0
    add_column :users, :sign_in_count, :integer, null: false, default: 0
    add_column :users, :failed_attempts, :integer, null: false, default: 0
    add_column :users, :unlock_token, :string, null: true, default: nil
  end
end
