class AddColumnToVisitorService < ActiveRecord::Migration[5.2]
  def change
    add_column :visitor_services, :code, :string, :unique => true
    add_column :visitor_services, :status, :integer, null: false, default: 0

  end
end
