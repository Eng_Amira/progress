class AddStatusToRequiredFiles < ActiveRecord::Migration[5.2]
  def change
    add_column :basic_data, :status, :integer, :default => 0
    add_column :required_files, :status, :integer, :default => 0
  end
end
