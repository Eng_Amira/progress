class CreateRequiredFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :required_files do |t|
      t.integer :user_id
      t.string :description

      t.timestamps
    end
  end
end
