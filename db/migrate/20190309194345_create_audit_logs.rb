class CreateAuditLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :audit_logs do |t|
      t.integer :user_id
      t.integer :added_by
      t.integer :foreign_id
      t.string :action_type
      t.string :meta_data
      t.string :ip_address
      t.string :user_agent

      t.timestamps
    end
  end
end
