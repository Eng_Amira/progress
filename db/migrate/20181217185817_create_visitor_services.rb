class CreateVisitorServices < ActiveRecord::Migration[5.2]
  def change
    create_table :visitor_services do |t|
      t.integer :service_id
      t.string :name
      t.string :company_type
      t.string :email
      t.string :phone
      t.string :address
      t.date :birth_date
      t.string :job_title
      t.string :adminstrator_name
      t.string :previous_work
      t.string :details

      t.timestamps
    end
  end
end
