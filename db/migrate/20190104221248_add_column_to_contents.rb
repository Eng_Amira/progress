class AddColumnToContents < ActiveRecord::Migration[5.2]
  def change
    add_column :contents, :title_en, :string
    add_column :contents, :description_en, :text
    rename_column :contents, :title, :title_ar
    rename_column :contents, :description, :description_ar


  end
end
