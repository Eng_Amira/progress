class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.integer :user_id
      t.integer :ticket_id
      t.string :ticket_number
      t.string :title
      t.text :description
      t.integer :status, :default => 0
      t.integer :type, :default => 0

      t.timestamps
    end
  end
end
