class RenameColumnFromPremiumCustomers < ActiveRecord::Migration[5.2]
  def change
    add_column :premium_customers, :description_ar, :text
    add_column :premium_customers, :description_en, :text
    rename_column :premium_customers, :name, :name_ar
    rename_column :premium_customers, :details, :name_en
  end
end
