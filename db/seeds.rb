# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create([{
  email: "amira.elfayome@servicehigh.com",
  password: "amira123456",
  username: "Amira",
  role_id: 1,
  active: 1,
  remember_token: Clearance::Token.new,
}])

New.create([{
  title_ar: "الإستشارات المالية",
  title_en:  "Financial Consulting",
  description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
    <ul class='b'>
    <li>النظم المالية والمحاسبية</li><br>
    <li>مراجعة وتحسين العمليات المحاسبية</li><br>
    <li> أنظمة وأساليب التنبؤ</li><br>
    <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
    <li>محاسبة التكاليف.</li><br>
    <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
    </ul>",
  description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
    <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
    </ul>",
  status: 1,
},{
  title_ar: "الإستشارات المالية",
  title_en:  "Financial Consulting",
  description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
    <ul class='b'>
    <li>النظم المالية والمحاسبية</li><br>
    <li>مراجعة وتحسين العمليات المحاسبية</li><br>
    <li> أنظمة وأساليب التنبؤ</li><br>
    <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
    <li>محاسبة التكاليف.</li><br>
    <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
    </ul>",
  description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
    <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
    </ul>",
  status: 1,

}])
Content.create([{
  key: "Address",
  category: "Contact_us",
  title_ar: "جمهورية مصر العربية - القاهرة",
  title_en:  "Arab Rebublic Of Egypt - Cairo",
  description_ar: "شارع المعز لدين الله الفاطمى ",
  description_en: "12 Al Moez Ldin Allah Al Fatmi",
},
{
  key: "contacts",
  category: "Contact_us",
  title_ar: "(002)01007460059",
  title_en:  "engamira333@gmail.com",
  description_ar: " يوميا من الأحد إلى الخميس من الساعة التاسعة صباحا حتى الخامسة عصرا ",
  description_en: "from sunday to thursday from 9AM to 5PM",
  
},
{
  key: "map",
  category: "Contact_us",
  title_ar: "",
  title_en:  "",
  description_ar: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.4342194736973!2d31.259848215115312!3d30.05308558187922!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1458409dfc862713%3A0x71140bd728c381!2s12+Al+Moez+Ldin+Allah+Al+Fatmi%2C+El-Gamaleya%2C+Qism+El-Gamaleya%2C+Cairo+Governorate!5e0!3m2!1sen!2seg!4v1549638078529",
  description_en: "",
  
},{
    key: "About Us",
    category: "About_Us",
    title_ar: "لمحة عنا",
    title_en:  "About Us",
    description_ar: "<p><strong>تأسست شركة استشارات التقدم للإستشارات عام 2019 لتقدم أفضل</strong> خدمات الإستشارات الإدارية للشركات والمؤسسات المحلية الدولية وفقاً لأفضل الممارسات المتعارف عليها في هذا المجال.<br> ومنذ نشأتها أخذت الشركة على عاتقها التوسع المستمر لتصبح واحدة من كبريات الشركات الاستشارية الرائدة في المنطقة من خلال مكاتبها العاملة المنتشرة في دول العالم العربي إلى جانب مكاتب التمثيل في أوروبا وآسيا وأمريكا الشمالية  <br> حيث يقدم مستشارونا في مجالات الإدارة سلسلة متكاملة من الخدمات عالية الجودة لتلبية إحتياجات مجتمع الأعمال. <br> حققت الشركة عبر العقود الثلاثة الأخيرة سمعة متميزة في العديد من المجالات ذات العلاقة بالإستشارات الإدارية والمالية والأنظمة الإدارية وأنظمة المعلومات وأنظمة الجودة الإدارية وأنظمة الإدارة البيئية </p><br>",
    description_en: "<p><strong>Progress-Consult is dedicated to providing the best quality of consulting services,</strong> that include business and investment advisory services,<br> restructuring and other organizational services, financial consulting, management system standards,<br> and privatization services.<br> With its multi-disciplinary professionals working across the Middle East and many other parts of the world,<br> to provide high-quality services for both private and public sectors, <br>Progress-Consult succeeded in introducing innovative methodologies for businesses and governmental bodies alike where progress and growth became <br>a necessity in the era of accelerated globalization <br>. Our outstanding results and testimonies of our clients affirm the innovative, well-conceived, and result-oriented nature of our methodologies.</p><br>",
            },
  {
    key: "Our Company",
    category: "About_Us",
    title_ar: "عن الشركة",
    title_en:  "About Our Company",
    description_ar: "<p><strong>تأسست شركة استشارات التقدم للإستشارات عام 2019 لتقدم أفضل</strong> خدمات الإستشارات الإدارية للشركات والمؤسسات المحلية الدولية وفقاً لأفضل الممارسات المتعارف عليها في هذا المجال.<br> ومنذ نشأتها أخذت الشركة على عاتقها التوسع المستمر لتصبح واحدة من كبريات الشركات الاستشارية الرائدة في المنطقة من خلال مكاتبها العاملة المنتشرة في دول العالم العربي إلى جانب مكاتب التمثيل في أوروبا وآسيا وأمريكا الشمالية  <br> حيث يقدم مستشارونا في مجالات الإدارة سلسلة متكاملة من الخدمات عالية الجودة لتلبية إحتياجات مجتمع الأعمال. <br> حققت الشركة عبر العقود الثلاثة الأخيرة سمعة متميزة في العديد من المجالات ذات العلاقة بالإستشارات الإدارية والمالية والأنظمة الإدارية وأنظمة المعلومات وأنظمة الجودة الإدارية وأنظمة الإدارة البيئية </p><br>",
    description_en: "<p><strong>Progress-Consult is dedicated to providing the best quality of consulting services,</strong> that include business and investment advisory services,<br> restructuring and other organizational services, financial consulting, management system standards,<br> and privatization services.<br> With its multi-disciplinary professionals working across the Middle East and many other parts of the world,<br> to provide high-quality services for both private and public sectors, <br>Progress-Consult succeeded in introducing innovative methodologies for businesses and governmental bodies alike where progress and growth became <br>a necessity in the era of accelerated globalization <br>. Our outstanding results and testimonies of our clients affirm the innovative, well-conceived, and result-oriented nature of our methodologies.</p><br>",
          
      },
  {
    key: "Human_Cadres",
    category: "Human_Cadres",
    title_ar: "لمحة عن كوادرنا البشرية",
    title_en:  "our Human Cadres",
    description_ar: "<p class='lead'>
    منذ تأسيس الشركة ونحن نعمل بإستمرار على الحفاظ على مستوى أداءنا المهنى وما يترتب عليه من تطوير الإبداع وتنمية كوادرنا البشرية . و نحن نركز على توظيف الأشخاص ذو المهارات المهنية و التقنيات الإدارية العالية و التى من خلالها نحقق الأداء بامتيازللاستمرار فى تنمية اعمالنا و تفعيل دورنا القيادى.
  </p>
  <p class='lead'>
  نوفر بيئة عمل صحية مدعمة بالدورات التدريبية الحديثة والبرامج التنموية اللازمة لتعزيز قدرات الموظفين الإبداعية و خبراتهم العلمية حتى يتسنى لهم تقديم العون والنصيحة لخدمة عملائنا الأعزاء وتوطيد العلاقات مع شركائنا.              
  </p>
  <p class='lead'>
  كل فرد فى الشركة يلعب دور اساسى و فعال فى منظومة عمل متكاملة تتميز بالتناغم ، والتعاون و الالتزام . إن موظفينا على درجة عالية من المعرفة و الابداع مركزين جهودهم لابتكار نماذج بيع اكثر فاعلية واسرع فى الأداء مركزين على معايير إرضاء العميل و على دعم قاعدة الشراكة بيننا و خلق فرص متنامية لكل شركاء المنظومة.
  </p>",
    description_en: "<p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>",
            },
    {
    key: "Financial Consulting",
    category: "Our_Services",
    title_ar: "الإستشارات المالية",
    title_en:  "Financial Consulting",
    description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
    <ul class='b'>
    <li>النظم المالية والمحاسبية</li><br>
    <li>مراجعة وتحسين العمليات المحاسبية</li><br>
    <li> أنظمة وأساليب التنبؤ</li><br>
    <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
    <li>محاسبة التكاليف.</li><br>
    <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
  </ul>",
    description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
    <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
    </ul>",
            },
    {
    key: "Human Resources Consulting Services",
    category: "Our_Services",
    title_ar: "استشارات الموارد البشرية",
    title_en:  "Human Resources Consulting Services",
    description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
    <ul class='b'>
    <li>النظم المالية والمحاسبية</li><br>
    <li>مراجعة وتحسين العمليات المحاسبية</li><br>
    <li> أنظمة وأساليب التنبؤ</li><br>
    <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
    <li>محاسبة التكاليف.</li><br>
    <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
  </ul>",
    description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
    <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
    </ul>",
            },
    {
    key: "Business and Investment Advisory Services",
    category: "Our_Services",
    title_ar: "الاستشارات الإدارية في مجال الاستثمار والأعمال التجارية",
    title_en:  "Business and Investment Advisory Services",
    description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
    <ul class='b'>
    <li>النظم المالية والمحاسبية</li><br>
    <li>مراجعة وتحسين العمليات المحاسبية</li><br>
    <li> أنظمة وأساليب التنبؤ</li><br>
    <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
    <li>محاسبة التكاليف.</li><br>
    <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
  </ul>",
    description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
    <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
    </ul>",
            },
    {
    key: "Organization and Restructuring",
    category: "Our_Services",
    title_ar: "خدمات التنظيم وإعادة الهيكلة",
    title_en:  "Organization and Restructuring",
    description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
    <ul class='b'>
    <li>النظم المالية والمحاسبية</li><br>
    <li>مراجعة وتحسين العمليات المحاسبية</li><br>
    <li> أنظمة وأساليب التنبؤ</li><br>
    <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
    <li>محاسبة التكاليف.</li><br>
    <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
  </ul>",
    description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
    <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
    </ul>",
            },
      {
      key: "Management System Standards",
      category: "Our_Services",
      title_ar: "مواصفات نظم الإدارة",
      title_en:  "Management System Standards (MSS)",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
    </ul>",
      description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
                  },
    {
    key: "Customers",
    category: "Our_Customers",
    title_ar: "عميل 1",
    title_en:  "Customer 1",
    description_ar: "<p class='lead'>
    منذ تأسيس الشركة ونحن نعمل بإستمرار على الحفاظ على مستوى أداءنا المهنى وما يترتب عليه من تطوير الإبداع وتنمية كوادرنا البشرية . و نحن نركز على توظيف الأشخاص ذو المهارات المهنية و التقنيات الإدارية العالية و التى من خلالها نحقق الأداء بامتيازللاستمرار فى تنمية اعمالنا و تفعيل دورنا القيادى.
  </p>
  <p class='lead'>
  نوفر بيئة عمل صحية مدعمة بالدورات التدريبية الحديثة والبرامج التنموية اللازمة لتعزيز قدرات الموظفين الإبداعية و خبراتهم العلمية حتى يتسنى لهم تقديم العون والنصيحة لخدمة عملائنا الأعزاء وتوطيد العلاقات مع شركائنا.              
  </p>
  <p class='lead'>
  كل فرد فى الشركة يلعب دور اساسى و فعال فى منظومة عمل متكاملة تتميز بالتناغم ، والتعاون و الالتزام . إن موظفينا على درجة عالية من المعرفة و الابداع مركزين جهودهم لابتكار نماذج بيع اكثر فاعلية واسرع فى الأداء مركزين على معايير إرضاء العميل و على دعم قاعدة الشراكة بيننا و خلق فرص متنامية لكل شركاء المنظومة.
  </p>",
    description_en: "<p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>",
            },
           
    {
    key: "Customers",
    category: "Our_Customers",
    title_ar: "عميل 2",
    title_en:  "Customer 2",
    description_ar: "<p class='lead'>
    منذ تأسيس الشركة ونحن نعمل بإستمرار على الحفاظ على مستوى أداءنا المهنى وما يترتب عليه من تطوير الإبداع وتنمية كوادرنا البشرية . و نحن نركز على توظيف الأشخاص ذو المهارات المهنية و التقنيات الإدارية العالية و التى من خلالها نحقق الأداء بامتيازللاستمرار فى تنمية اعمالنا و تفعيل دورنا القيادى.
  </p>
  <p class='lead'>
  نوفر بيئة عمل صحية مدعمة بالدورات التدريبية الحديثة والبرامج التنموية اللازمة لتعزيز قدرات الموظفين الإبداعية و خبراتهم العلمية حتى يتسنى لهم تقديم العون والنصيحة لخدمة عملائنا الأعزاء وتوطيد العلاقات مع شركائنا.              
  </p>
  <p class='lead'>
  كل فرد فى الشركة يلعب دور اساسى و فعال فى منظومة عمل متكاملة تتميز بالتناغم ، والتعاون و الالتزام . إن موظفينا على درجة عالية من المعرفة و الابداع مركزين جهودهم لابتكار نماذج بيع اكثر فاعلية واسرع فى الأداء مركزين على معايير إرضاء العميل و على دعم قاعدة الشراكة بيننا و خلق فرص متنامية لكل شركاء المنظومة.
  </p>",
    description_en: "<p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>",
            },
          
    {
    key: "Customers",
    category: "Our_Customers",
    title_ar: "عميل 3",
    title_en:  "Customer 3",
    description_ar: "<p class='lead'>
    منذ تأسيس الشركة ونحن نعمل بإستمرار على الحفاظ على مستوى أداءنا المهنى وما يترتب عليه من تطوير الإبداع وتنمية كوادرنا البشرية . و نحن نركز على توظيف الأشخاص ذو المهارات المهنية و التقنيات الإدارية العالية و التى من خلالها نحقق الأداء بامتيازللاستمرار فى تنمية اعمالنا و تفعيل دورنا القيادى.
  </p>
  <p class='lead'>
  نوفر بيئة عمل صحية مدعمة بالدورات التدريبية الحديثة والبرامج التنموية اللازمة لتعزيز قدرات الموظفين الإبداعية و خبراتهم العلمية حتى يتسنى لهم تقديم العون والنصيحة لخدمة عملائنا الأعزاء وتوطيد العلاقات مع شركائنا.              
  </p>
  <p class='lead'>
  كل فرد فى الشركة يلعب دور اساسى و فعال فى منظومة عمل متكاملة تتميز بالتناغم ، والتعاون و الالتزام . إن موظفينا على درجة عالية من المعرفة و الابداع مركزين جهودهم لابتكار نماذج بيع اكثر فاعلية واسرع فى الأداء مركزين على معايير إرضاء العميل و على دعم قاعدة الشراكة بيننا و خلق فرص متنامية لكل شركاء المنظومة.
  </p>",
    description_en: "<p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>",
            },
           
    {
    key: "Customers",
    category: "Our_Customers",
    title_ar: "عميل 4",
    title_en:  "Customer 4",
    description_ar: "<p class='lead'>
    منذ تأسيس الشركة ونحن نعمل بإستمرار على الحفاظ على مستوى أداءنا المهنى وما يترتب عليه من تطوير الإبداع وتنمية كوادرنا البشرية . و نحن نركز على توظيف الأشخاص ذو المهارات المهنية و التقنيات الإدارية العالية و التى من خلالها نحقق الأداء بامتيازللاستمرار فى تنمية اعمالنا و تفعيل دورنا القيادى.
  </p>
  <p class='lead'>
  نوفر بيئة عمل صحية مدعمة بالدورات التدريبية الحديثة والبرامج التنموية اللازمة لتعزيز قدرات الموظفين الإبداعية و خبراتهم العلمية حتى يتسنى لهم تقديم العون والنصيحة لخدمة عملائنا الأعزاء وتوطيد العلاقات مع شركائنا.              
  </p>
  <p class='lead'>
  كل فرد فى الشركة يلعب دور اساسى و فعال فى منظومة عمل متكاملة تتميز بالتناغم ، والتعاون و الالتزام . إن موظفينا على درجة عالية من المعرفة و الابداع مركزين جهودهم لابتكار نماذج بيع اكثر فاعلية واسرع فى الأداء مركزين على معايير إرضاء العميل و على دعم قاعدة الشراكة بيننا و خلق فرص متنامية لكل شركاء المنظومة.
  </p>",
    description_en: "<p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>",
            },
          
    {
    key: "Customers",
    category: "Our_Customers",
    title_ar: "عميل 5",
    title_en:  "Customer 5",
    description_ar: "<p class='lead'>
    منذ تأسيس الشركة ونحن نعمل بإستمرار على الحفاظ على مستوى أداءنا المهنى وما يترتب عليه من تطوير الإبداع وتنمية كوادرنا البشرية . و نحن نركز على توظيف الأشخاص ذو المهارات المهنية و التقنيات الإدارية العالية و التى من خلالها نحقق الأداء بامتيازللاستمرار فى تنمية اعمالنا و تفعيل دورنا القيادى.
  </p>
  <p class='lead'>
  نوفر بيئة عمل صحية مدعمة بالدورات التدريبية الحديثة والبرامج التنموية اللازمة لتعزيز قدرات الموظفين الإبداعية و خبراتهم العلمية حتى يتسنى لهم تقديم العون والنصيحة لخدمة عملائنا الأعزاء وتوطيد العلاقات مع شركائنا.              
  </p>
  <p class='lead'>
  كل فرد فى الشركة يلعب دور اساسى و فعال فى منظومة عمل متكاملة تتميز بالتناغم ، والتعاون و الالتزام . إن موظفينا على درجة عالية من المعرفة و الابداع مركزين جهودهم لابتكار نماذج بيع اكثر فاعلية واسرع فى الأداء مركزين على معايير إرضاء العميل و على دعم قاعدة الشراكة بيننا و خلق فرص متنامية لكل شركاء المنظومة.
  </p>",
    description_en: "<p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>",
            },
            
    {
    key: "Customers",
    category: "Our_Customers",
    title_ar: "عميل 6",
    title_en:  "Customer 6",
    description_ar: "<p class='lead'>
    منذ تأسيس الشركة ونحن نعمل بإستمرار على الحفاظ على مستوى أداءنا المهنى وما يترتب عليه من تطوير الإبداع وتنمية كوادرنا البشرية . و نحن نركز على توظيف الأشخاص ذو المهارات المهنية و التقنيات الإدارية العالية و التى من خلالها نحقق الأداء بامتيازللاستمرار فى تنمية اعمالنا و تفعيل دورنا القيادى.
  </p>
  <p class='lead'>
  نوفر بيئة عمل صحية مدعمة بالدورات التدريبية الحديثة والبرامج التنموية اللازمة لتعزيز قدرات الموظفين الإبداعية و خبراتهم العلمية حتى يتسنى لهم تقديم العون والنصيحة لخدمة عملائنا الأعزاء وتوطيد العلاقات مع شركائنا.              
  </p>
  <p class='lead'>
  كل فرد فى الشركة يلعب دور اساسى و فعال فى منظومة عمل متكاملة تتميز بالتناغم ، والتعاون و الالتزام . إن موظفينا على درجة عالية من المعرفة و الابداع مركزين جهودهم لابتكار نماذج بيع اكثر فاعلية واسرع فى الأداء مركزين على معايير إرضاء العميل و على دعم قاعدة الشراكة بيننا و خلق فرص متنامية لكل شركاء المنظومة.
  </p>",
    description_en: "<p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>",
            },
            
    {
    key: "Customers",
    category: "Our_Customers",
    title_ar: "عميل 7",
    title_en:  "Customer 7",
    description_ar: "<p class='lead'>
    منذ تأسيس الشركة ونحن نعمل بإستمرار على الحفاظ على مستوى أداءنا المهنى وما يترتب عليه من تطوير الإبداع وتنمية كوادرنا البشرية . و نحن نركز على توظيف الأشخاص ذو المهارات المهنية و التقنيات الإدارية العالية و التى من خلالها نحقق الأداء بامتيازللاستمرار فى تنمية اعمالنا و تفعيل دورنا القيادى.
  </p>
  <p class='lead'>
  نوفر بيئة عمل صحية مدعمة بالدورات التدريبية الحديثة والبرامج التنموية اللازمة لتعزيز قدرات الموظفين الإبداعية و خبراتهم العلمية حتى يتسنى لهم تقديم العون والنصيحة لخدمة عملائنا الأعزاء وتوطيد العلاقات مع شركائنا.              
  </p>
  <p class='lead'>
  كل فرد فى الشركة يلعب دور اساسى و فعال فى منظومة عمل متكاملة تتميز بالتناغم ، والتعاون و الالتزام . إن موظفينا على درجة عالية من المعرفة و الابداع مركزين جهودهم لابتكار نماذج بيع اكثر فاعلية واسرع فى الأداء مركزين على معايير إرضاء العميل و على دعم قاعدة الشراكة بيننا و خلق فرص متنامية لكل شركاء المنظومة.
  </p>",
    description_en: "<p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>
  <p class='lead'>
    Our experienced teams offer financial professionals an outstanding level of service. No matter what stage of the business growth cycle you're at or what issues you're facing, our specialist support teams tailor their knowledge to help you get the best possible outcome.
  </p>",
            },
  {
    key: "privacy_policy",
    category: "public_pages",
    title_ar: "سياسة الخصوصية",
    title_en:  "privacy Policy",
    description_ar: "<p>نحن نريد لزوارنا أن يكونوا مدركين لأي معلومات نقوم بجمعها، كيف نقوم باستخدامها، و تحت أي ظروف (إن وجدت)، نقوم بالكشف عنها.</p>

    <p>الرجاء كونوا متأكدين بأن خصوصيتكم مهمة لنا. حتى نتمكن من حماية معلوماتك الشخصية، يطلب منكم قراءة سياسة الخصوصية هذه قبل استعمال الموقع الالكتروني للمجموعة &nbsp;.</p>
    
    <p>إذا لم توافقوا على بنود و شروط سياسة الخصوصية بدون قيود، يتوجب عليكم فورا إنهاء استخدامكم لموقعنا الالكتروني على شبكة الانترنت.</p>
    
    <p>بشكل عام، تستطيع زيارة موقعنا الالكتروني دون أن تخبرنا من أنت أو أن تكشف معلومات عن نفسك. إلا أن هناك بعض الأحيان، نحتاج إلى معلومات منك مثل اسمك و عنوان بريدك الإلكتروني و ذلك من أجل مراسلتك أو تزويدك باشتراك مثلاً.</p>
    
    <p>أذا رغبت بتزويدنا بمعلومات عن طريق الانترنت، فإننا نرغب بإبلاغك كيف سنتعامل مع مثل هذه المعلومات. تقر أن استخدامك لهذا الموقع الالكتروني يعني موافقتك على هذه السياسة. إذا كنت لا توافق على هذه السياسة بالكامل، فان دخولك لهذا الموقع يعتبر غير مصرح به و عليك وقف استعمال هذا الموقع فوراً. تقر بأننا تحتفظ بحقنا بإرادتنا المنفردة و بدون أي تحديد من جهتنا بتعديل و تغيير شروط و بنود اتفاقية الخصوصية هذه في أي وقت.</p>
    
    <p>و ستكون ملزماً بأية تغييرات أو تعديلات بمجرد نشرها على الموقع الإلكتروني للمجموعة و ذلك عندما ترى المجموعة ذلك مناسباً.</p>
    
    <p>و لذلك فإننا ننصح بان تستمر بزيارة الموقع الإلكتروني للمجموعة&nbsp; من وقت إلى آخر لتتعرف على أية تغييرات من الممكن حصولها في المستقبل حول سياسة الخصوصية. قد تقوم المجموعة&nbsp;بتبليغكم عن أي تغييرات مادية من الممكن أن تؤثر على حقوقكم عن طريق بريدكم الالكتروني.</p>
    
    <p>لهذا ننصحكم أن تبقوا بريدكم الالكتروني يعمل و في حالة تغييره أن تقوموا بتبليغنا فوراً بعنوان بريدكم الإلكتروني الجديد.</p>
    ",
    description_en: "<p>
    <span  class='text'>In order for TAG-Consult to protect your personal information, you are requested to read this privacy policy prior to using TAG-Consult website located at <a href='client2.html'>www.progress-consultants.com</a> <br>
    <br>
    In general, you can visit our web sites without telling us who you are or revealing any information about yourself. There are times, however, when we may need information from you, such as your name and e-mail address, to correspond with you or provide you with a subscription, for example. If you choose to give us personal information via the Internet, it is our intent to let you know how we will use such information. <br>
    <br>
    You acknowledge that TAG-Consult reserves the right solely and without any limitation on its part to modify and amend the terms and conditions of this privacy policy at any time. You will be bound by such amendments and modifications as soon as they are posted on TAG-Consult website when TAG-Consult may deem that appropriate. <br>
    <br>
    In this situation, we advise you to keep visiting TAG-Consult website from time to time to be familiar with any changes that may take place in the future in relation to this privacy policy.<br>
    <br>
    TAG-Consult may choose to notify you of any material changes that may affect your rights via your e-mail, we advise you to keep your e-mail effective and in the event that it has been changed to notify us immediately of your new e-mail address. <br>
    <br>
    <br>
    1. Why did we adopt this Privacy policy?<br>
    <br>
    TAG-Consult has adopted this Privacy Policy to inform users of the kind of personal information that will be collected, the purpose of collecting it and the protection measures that TAG-Consult has employed or will further employ in the future to keep your personal information confidential and private. <br>
    <br>
    2. What kind of information do we collect form you? <br>
    <br>
    In order to subscribe to our website, submit a comment, or contact us, you agree to provide the following information in the required form: <br>
    <br>
    • Your name <br>
    • Your Company name <br>
    • Your E-mail address <br>
    • Your address <br>
    <br>
    3. Why did we collect this information?<br>
    TAG-Consult collects your personal information for the following reason: <br>
    <br>
    1- To contact you. <br>
    2- Submitional Reasons <br>
    3- Statistical reasons. <br>
    <br>
    4. Are we going to disclose your information to other parties not mentioned here?<br>
    TAG-Consult will not disclose your personal information to any third party, unless it receives an order from a competent court or required to do so under the provisions of Law. <br>
    <br>
    5. What security measures do we use to protect your personal information? <br>
    <br>
    TAG-Consult saves the records of its members on high quality servers, database, and safety systems. TAG-Consult has employed several technological security measures to protect your personal and confidential information from being misused, altered while being transmitted over the Internet. Moreover, TAG-Consult uses cookies to gather information that will improve its website and serve you efficiently. Cookies are small files that the website places on the website user computer which allow the website server to recognize the user each time he visits the website and store the user preferences. <br>
    However, TAG-Consult does not guarantee in any way that its use of the above mentioned security measures would guarantee fully that your personal information would remain private while being transferred over the Internet. <br>
    <br>
    6. Whom should you ask if you have any other questions? <br>
    If you have any question related to this privacy policy or any of its provision or you wish to make any comment about it, Please don’t hesitate to <a href='call.html'>contact us</a></span>
  </p>",
  },
  {
    key: "terms_of_use",
    category: "public_pages",
    title_ar: "سياسة الاستخدام",
    title_en:  "terms of user",
    description_ar: "<p>نحن نريد لزوارنا أن يكونوا مدركين لأي معلومات نقوم بجمعها، كيف نقوم باستخدامها، و تحت أي ظروف (إن وجدت)، نقوم بالكشف عنها.</p>

    <p>الرجاء كونوا متأكدين بأن خصوصيتكم مهمة لنا. حتى نتمكن من حماية معلوماتك الشخصية، يطلب منكم قراءة سياسة الخصوصية هذه قبل استعمال الموقع الالكتروني للمجموعة &nbsp;.</p>
    
    <p>إذا لم توافقوا على بنود و شروط سياسة الخصوصية بدون قيود، يتوجب عليكم فورا إنهاء استخدامكم لموقعنا الالكتروني على شبكة الانترنت.</p>
    
    <p>بشكل عام، تستطيع زيارة موقعنا الالكتروني دون أن تخبرنا من أنت أو أن تكشف معلومات عن نفسك. إلا أن هناك بعض الأحيان، نحتاج إلى معلومات منك مثل اسمك و عنوان بريدك الإلكتروني و ذلك من أجل مراسلتك أو تزويدك باشتراك مثلاً.</p>
    
    <p>أذا رغبت بتزويدنا بمعلومات عن طريق الانترنت، فإننا نرغب بإبلاغك كيف سنتعامل مع مثل هذه المعلومات. تقر أن استخدامك لهذا الموقع الالكتروني يعني موافقتك على هذه السياسة. إذا كنت لا توافق على هذه السياسة بالكامل، فان دخولك لهذا الموقع يعتبر غير مصرح به و عليك وقف استعمال هذا الموقع فوراً. تقر بأننا تحتفظ بحقنا بإرادتنا المنفردة و بدون أي تحديد من جهتنا بتعديل و تغيير شروط و بنود اتفاقية الخصوصية هذه في أي وقت.</p>
    
    <p>و ستكون ملزماً بأية تغييرات أو تعديلات بمجرد نشرها على الموقع الإلكتروني للمجموعة و ذلك عندما ترى المجموعة ذلك مناسباً.</p>
    
    <p>و لذلك فإننا ننصح بان تستمر بزيارة الموقع الإلكتروني للمجموعة&nbsp; من وقت إلى آخر لتتعرف على أية تغييرات من الممكن حصولها في المستقبل حول سياسة الخصوصية. قد تقوم المجموعة&nbsp;بتبليغكم عن أي تغييرات مادية من الممكن أن تؤثر على حقوقكم عن طريق بريدكم الالكتروني.</p>
    
    <p>لهذا ننصحكم أن تبقوا بريدكم الالكتروني يعمل و في حالة تغييره أن تقوموا بتبليغنا فوراً بعنوان بريدكم الإلكتروني الجديد.</p>
    ",
    description_en: "<p>
    <span  class='text'>In order for TAG-Consult to protect your personal information, you are requested to read this privacy policy prior to using TAG-Consult website located at <a href='client2.html'>www.progress-consultants.com</a> <br>
    <br>
    In general, you can visit our web sites without telling us who you are or revealing any information about yourself. There are times, however, when we may need information from you, such as your name and e-mail address, to correspond with you or provide you with a subscription, for example. If you choose to give us personal information via the Internet, it is our intent to let you know how we will use such information. <br>
    <br>
    You acknowledge that TAG-Consult reserves the right solely and without any limitation on its part to modify and amend the terms and conditions of this privacy policy at any time. You will be bound by such amendments and modifications as soon as they are posted on TAG-Consult website when TAG-Consult may deem that appropriate. <br>
    <br>
    In this situation, we advise you to keep visiting TAG-Consult website from time to time to be familiar with any changes that may take place in the future in relation to this privacy policy.<br>
    <br>
    TAG-Consult may choose to notify you of any material changes that may affect your rights via your e-mail, we advise you to keep your e-mail effective and in the event that it has been changed to notify us immediately of your new e-mail address. <br>
    <br>
    <br>
    1. Why did we adopt this Privacy policy?<br>
    <br>
    TAG-Consult has adopted this Privacy Policy to inform users of the kind of personal information that will be collected, the purpose of collecting it and the protection measures that TAG-Consult has employed or will further employ in the future to keep your personal information confidential and private. <br>
    <br>
    2. What kind of information do we collect form you? <br>
    <br>
    In order to subscribe to our website, submit a comment, or contact us, you agree to provide the following information in the required form: <br>
    <br>
    • Your name <br>
    • Your Company name <br>
    • Your E-mail address <br>
    • Your address <br>
    <br>
    3. Why did we collect this information?<br>
    TAG-Consult collects your personal information for the following reason: <br>
    <br>
    1- To contact you. <br>
    2- Submitional Reasons <br>
    3- Statistical reasons. <br>
    <br>
    4. Are we going to disclose your information to other parties not mentioned here?<br>
    TAG-Consult will not disclose your personal information to any third party, unless it receives an order from a competent court or required to do so under the provisions of Law. <br>
    <br>
    5. What security measures do we use to protect your personal information? <br>
    <br>
    TAG-Consult saves the records of its members on high quality servers, database, and safety systems. TAG-Consult has employed several technological security measures to protect your personal and confidential information from being misused, altered while being transmitted over the Internet. Moreover, TAG-Consult uses cookies to gather information that will improve its website and serve you efficiently. Cookies are small files that the website places on the website user computer which allow the website server to recognize the user each time he visits the website and store the user preferences. <br>
    However, TAG-Consult does not guarantee in any way that its use of the above mentioned security measures would guarantee fully that your personal information would remain private while being transferred over the Internet. <br>
    <br>
    6. Whom should you ask if you have any other questions? <br>
    If you have any question related to this privacy policy or any of its provision or you wish to make any comment about it, Please don’t hesitate to <a href='call.html'>contact us</a></span>
  </p>",
  },
  {
    key: "disclaimer",
    category: "public_pages",
    title_ar: "إخلاء المسئولية",
    title_en:  "Disclaimer",
    description_ar: "<p>نحن نريد لزوارنا أن يكونوا مدركين لأي معلومات نقوم بجمعها، كيف نقوم باستخدامها، و تحت أي ظروف (إن وجدت)، نقوم بالكشف عنها.</p>

    <p>الرجاء كونوا متأكدين بأن خصوصيتكم مهمة لنا. حتى نتمكن من حماية معلوماتك الشخصية، يطلب منكم قراءة سياسة الخصوصية هذه قبل استعمال الموقع الالكتروني للمجموعة &nbsp;.</p>
    
    <p>إذا لم توافقوا على بنود و شروط سياسة الخصوصية بدون قيود، يتوجب عليكم فورا إنهاء استخدامكم لموقعنا الالكتروني على شبكة الانترنت.</p>
    
    <p>بشكل عام، تستطيع زيارة موقعنا الالكتروني دون أن تخبرنا من أنت أو أن تكشف معلومات عن نفسك. إلا أن هناك بعض الأحيان، نحتاج إلى معلومات منك مثل اسمك و عنوان بريدك الإلكتروني و ذلك من أجل مراسلتك أو تزويدك باشتراك مثلاً.</p>
    
    <p>أذا رغبت بتزويدنا بمعلومات عن طريق الانترنت، فإننا نرغب بإبلاغك كيف سنتعامل مع مثل هذه المعلومات. تقر أن استخدامك لهذا الموقع الالكتروني يعني موافقتك على هذه السياسة. إذا كنت لا توافق على هذه السياسة بالكامل، فان دخولك لهذا الموقع يعتبر غير مصرح به و عليك وقف استعمال هذا الموقع فوراً. تقر بأننا تحتفظ بحقنا بإرادتنا المنفردة و بدون أي تحديد من جهتنا بتعديل و تغيير شروط و بنود اتفاقية الخصوصية هذه في أي وقت.</p>
    
    <p>و ستكون ملزماً بأية تغييرات أو تعديلات بمجرد نشرها على الموقع الإلكتروني للمجموعة و ذلك عندما ترى المجموعة ذلك مناسباً.</p>
    
    <p>و لذلك فإننا ننصح بان تستمر بزيارة الموقع الإلكتروني للمجموعة&nbsp; من وقت إلى آخر لتتعرف على أية تغييرات من الممكن حصولها في المستقبل حول سياسة الخصوصية. قد تقوم المجموعة&nbsp;بتبليغكم عن أي تغييرات مادية من الممكن أن تؤثر على حقوقكم عن طريق بريدكم الالكتروني.</p>
    
    <p>لهذا ننصحكم أن تبقوا بريدكم الالكتروني يعمل و في حالة تغييره أن تقوموا بتبليغنا فوراً بعنوان بريدكم الإلكتروني الجديد.</p>
    ",
    description_en: "<p>
    <span  class='text'>In order for TAG-Consult to protect your personal information, you are requested to read this privacy policy prior to using TAG-Consult website located at <a href='client2.html'>www.progress-consultants.com</a> <br>
    <br>
    In general, you can visit our web sites without telling us who you are or revealing any information about yourself. There are times, however, when we may need information from you, such as your name and e-mail address, to correspond with you or provide you with a subscription, for example. If you choose to give us personal information via the Internet, it is our intent to let you know how we will use such information. <br>
    <br>
    You acknowledge that TAG-Consult reserves the right solely and without any limitation on its part to modify and amend the terms and conditions of this privacy policy at any time. You will be bound by such amendments and modifications as soon as they are posted on TAG-Consult website when TAG-Consult may deem that appropriate. <br>
    <br>
    In this situation, we advise you to keep visiting TAG-Consult website from time to time to be familiar with any changes that may take place in the future in relation to this privacy policy.<br>
    <br>
    TAG-Consult may choose to notify you of any material changes that may affect your rights via your e-mail, we advise you to keep your e-mail effective and in the event that it has been changed to notify us immediately of your new e-mail address. <br>
    <br>
    <br>
    1. Why did we adopt this Privacy policy?<br>
    <br>
    TAG-Consult has adopted this Privacy Policy to inform users of the kind of personal information that will be collected, the purpose of collecting it and the protection measures that TAG-Consult has employed or will further employ in the future to keep your personal information confidential and private. <br>
    <br>
    2. What kind of information do we collect form you? <br>
    <br>
    In order to subscribe to our website, submit a comment, or contact us, you agree to provide the following information in the required form: <br>
    <br>
    • Your name <br>
    • Your Company name <br>
    • Your E-mail address <br>
    • Your address <br>
    <br>
    3. Why did we collect this information?<br>
    TAG-Consult collects your personal information for the following reason: <br>
    <br>
    1- To contact you. <br>
    2- Submitional Reasons <br>
    3- Statistical reasons. <br>
    <br>
    4. Are we going to disclose your information to other parties not mentioned here?<br>
    TAG-Consult will not disclose your personal information to any third party, unless it receives an order from a competent court or required to do so under the provisions of Law. <br>
    <br>
    5. What security measures do we use to protect your personal information? <br>
    <br>
    TAG-Consult saves the records of its members on high quality servers, database, and safety systems. TAG-Consult has employed several technological security measures to protect your personal and confidential information from being misused, altered while being transmitted over the Internet. Moreover, TAG-Consult uses cookies to gather information that will improve its website and serve you efficiently. Cookies are small files that the website places on the website user computer which allow the website server to recognize the user each time he visits the website and store the user preferences. <br>
    However, TAG-Consult does not guarantee in any way that its use of the above mentioned security measures would guarantee fully that your personal information would remain private while being transferred over the Internet. <br>
    <br>
    6. Whom should you ask if you have any other questions? <br>
    If you have any question related to this privacy policy or any of its provision or you wish to make any comment about it, Please don’t hesitate to <a href='call.html'>contact us</a></span>
  </p>",
  },
    
    ]) 

    OurMission.create([{
        title_ar: "الإستشارات المالية",
        description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
        <ul class='b'>
        <li>النظم المالية والمحاسبية</li><br>
        <li>مراجعة وتحسين العمليات المحاسبية</li><br>
        <li> أنظمة وأساليب التنبؤ</li><br>
        <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
        <li>محاسبة التكاليف.</li><br>
        <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
        title_en: "Financial Consulting",
        description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
        <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
        small_pic: "fa fa-briefcase"
        },
        {
        title_ar: "الإستشارات المالية",
        description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
        <ul class='b'>
        <li>النظم المالية والمحاسبية</li><br>
        <li>مراجعة وتحسين العمليات المحاسبية</li><br>
        <li> أنظمة وأساليب التنبؤ</li><br>
        <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
        <li>محاسبة التكاليف.</li><br>
        <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
        title_en: "Financial Consulting",
        description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
        <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
        small_pic: "fa fa-clipboard"
        },
        {
        title_ar: "الإستشارات المالية",
        description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
        <ul class='b'>
        <li>النظم المالية والمحاسبية</li><br>
        <li>مراجعة وتحسين العمليات المحاسبية</li><br>
        <li> أنظمة وأساليب التنبؤ</li><br>
        <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
        <li>محاسبة التكاليف.</li><br>
        <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
        title_en: "Financial Consulting",
        description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
        <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
        small_pic: "fa fa-file-text"
        },
        {
        title_ar: "الإستشارات المالية",
        description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
        <ul class='b'>
        <li>النظم المالية والمحاسبية</li><br>
        <li>مراجعة وتحسين العمليات المحاسبية</li><br>
        <li> أنظمة وأساليب التنبؤ</li><br>
        <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
        <li>محاسبة التكاليف.</li><br>
        <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
        title_en: "Financial Consulting",
        description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
        <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
        small_pic: "fa fa-bookmark-o"
        },
     {
        title_ar: "الإستشارات المالية",
        description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
        <ul class='b'>
        <li>النظم المالية والمحاسبية</li><br>
        <li>مراجعة وتحسين العمليات المحاسبية</li><br>
        <li> أنظمة وأساليب التنبؤ</li><br>
        <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
        <li>محاسبة التكاليف.</li><br>
        <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
        title_en: "Financial Consulting",
        description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
        <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
        small_pic: "fa fa-briefcase"
        },
        {
        title_ar: "الإستشارات المالية",
        description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
        <ul class='b'>
        <li>النظم المالية والمحاسبية</li><br>
        <li>مراجعة وتحسين العمليات المحاسبية</li><br>
        <li> أنظمة وأساليب التنبؤ</li><br>
        <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
        <li>محاسبة التكاليف.</li><br>
        <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
        title_en: "Financial Consulting",
        description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
        <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
        small_pic: "fa fa-clipboard"
        },
        {
        title_ar: "الإستشارات المالية",
        description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
        <ul class='b'>
        <li>النظم المالية والمحاسبية</li><br>
        <li>مراجعة وتحسين العمليات المحاسبية</li><br>
        <li> أنظمة وأساليب التنبؤ</li><br>
        <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
        <li>محاسبة التكاليف.</li><br>
        <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
        title_en: "Financial Consulting",
        description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
        <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
        small_pic: "fa fa-file-text"
        },
        {
        title_ar: "الإستشارات المالية",
        description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
        <ul class='b'>
        <li>النظم المالية والمحاسبية</li><br>
        <li>مراجعة وتحسين العمليات المحاسبية</li><br>
        <li> أنظمة وأساليب التنبؤ</li><br>
        <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
        <li>محاسبة التكاليف.</li><br>
        <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
        title_en: "Financial Consulting",
        description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
        <ul class='b'>
          <li>Financial and Accounting Systems .</li><br>
          <li>Reviewing and Improving Accounting Operations.</li><br>
          <li>Projection and Forecasts Systems.</li><br>
          <li>Financial and Operational Due Diligence Systems.</li><br>
          <li> Costing Systems .</li><br>               
          <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
        small_pic: "fa fa-bookmark-o"
        },
        {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-briefcase"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-clipboard"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-file-text"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-bookmark-o"
      },{
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-briefcase"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-clipboard"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-file-text"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-bookmark-o"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-briefcase"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-clipboard"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-file-text"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-bookmark-o"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-briefcase"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-clipboard"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-file-text"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-bookmark-o"
      },{
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-briefcase"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-clipboard"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-file-text"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-bookmark-o"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-briefcase"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-clipboard"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-file-text"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-bookmark-o"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-briefcase"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-clipboard"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-file-text"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-bookmark-o"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en: "<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-briefcase"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-clipboard"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-file-text"
      },
      {
      title_ar: "الإستشارات المالية",
      description_ar: "<p><strong>وهنا يتركز هدفنا الأساس على بناء نظام عمليات على أسس مالية سليمة.</strong> من شأنه تسهيل مسار الحركة المالية بحيث تكون قابلة لإدراجها في تقارير محاسبية ورفعها إلى الإدارة العليا,<br> ومن ضمن الخدمات التي نقدمهافي هذا المجال:.</p><br>
      <ul class='b'>
      <li>النظم المالية والمحاسبية</li><br>
      <li>مراجعة وتحسين العمليات المحاسبية</li><br>
      <li> أنظمة وأساليب التنبؤ</li><br>
      <li>أنظمة التحري للعمليات المالية والتشغيلية.</li><br>
      <li>محاسبة التكاليف.</li><br>
      <li>أنظمة تخفيض التكاليف ورصد الخسائر.</li><br>
      </ul>",
      title_en: "Financial Consulting",
      description_en:"<p><strong>With its connections, background and reputation in the financial community,</strong> we provides a broad variety of financial advisory services to its clients to help them develop a secure, efficient, and profitable financial system. Our primary goal is to provide financial tools, that would easily track financial events, provide financial information significant to the financial management of a company or an organization, and/or required for the preparation of financial statements. Whether automated or manual, we will consider all aspects of the financial system to help integrate and improve all procedures, controls, data, hardware, and support personnel involved within financial operations. Our services include, but are not limited to the following:</p><br>
      <ul class='b'>
            <li>Financial and Accounting Systems .</li><br>
            <li>Reviewing and Improving Accounting Operations.</li><br>
            <li>Projection and Forecasts Systems.</li><br>
            <li>Financial and Operational Due Diligence Systems.</li><br>
            <li> Costing Systems .</li><br>               
            <li>Loss and Cost Reduction Systems.</li><br>
      </ul>",
      small_pic: "fa fa-bookmark-o"
      },
])